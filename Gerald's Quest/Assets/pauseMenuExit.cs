﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/// <summary>
/// Function for the Pause menu exit button.
/// </summary>
public class pauseMenuExit : MonoBehaviour {

	private Button exit;

	/// <summary>
	/// Start this instance.
	/// </summary>
	void Start () {

		exit = (Button)GameObject.Find ("ExitButton").GetComponent<Button> ();
		exit.onClick.AddListener(() => exitClicked());
	}

	/// <summary>
	/// When Exits is clicked.
	/// </summary>
	public void exitClicked () {

		Debug.Log ("Exit lul");
		Application.Quit ();

	}
}
