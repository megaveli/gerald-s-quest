﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Score calculation. Uses the Scores script to get texts into the 
/// text boxes. 
/// </summary>
public class scoreCalc : MonoBehaviour {

	private int score;
	private int lowScore;
	private int lowScore2;
	private int lowScore3;

	private string prefName;
	private string lowName;
	private string lowName2;
	private string lowName3;

	private string lowScoreKey = "LowScore";
	private string lowScoreKey2 = "2nd";
	private string lowScoreKey3 = "3rd";

	private string lowNameKey = "LowScoreName";
	private string lowNameKey2 = "LowScoreName2";
	private string lowNameKey3 = "LowScoreName3";
	private string prefNameKey = "PlayerName";

	private Text yourScore;
	private Text low1;
	private Text low2;
	private Text low3;

	private GameObject playerScore;
	private Scores scores;


	/// <summary>
	/// Start this instance.
	/// </summary>
	void Start () {

		playerScore = GameObject.Find ("PlayerScore");
		scores = playerScore.GetComponent<Scores> ();

		lowScore = PlayerPrefs.GetInt(lowScoreKey,300); 
		lowScore2 = PlayerPrefs.GetInt(lowScoreKey2,400);
		lowScore3 = PlayerPrefs.GetInt(lowScoreKey3,500);

		score = scores.GetPlayerScore ();

		lowName = PlayerPrefs.GetString (lowNameKey, "Geraldina");
		lowName2 = PlayerPrefs.GetString (lowNameKey2, "Walter");
		lowName3 = PlayerPrefs.GetString (lowNameKey3, "Geraldo");

		prefName = PlayerPrefs.GetString (prefNameKey, "Default");

		yourScore = (Text)GameObject.Find ("yourScore").GetComponent<Text> ();
		low1 = (Text)GameObject.Find ("low1").GetComponent<Text> ();
		low2 = (Text)GameObject.Find ("low2").GetComponent<Text> ();
		low3 = (Text)GameObject.Find ("low3").GetComponent<Text> ();

		GetScore ();
	}


	/// <summary>
	/// Gets the scores into the text boxes.
	/// </summary>
	public void GetScore()
	{
		this.yourScore.text = "Your score is: " + score + "\n" + "\n" + "Your name: " + prefName; 
		this.low1.text = "1st " + lowScore + " points " + "  By: " + lowName;
		this.low2.text = "2nd " + lowScore2 + " points " + "  By: " + lowName2;
		this.low3.text = "3rd " + lowScore3 + " points " + "  By: " + lowName3;
	}

	void Update () {
		
	}
}
