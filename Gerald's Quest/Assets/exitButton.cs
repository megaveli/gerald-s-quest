﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/// <summary>
/// Exit button in the credits. Saves the scores before
/// quitting the application
/// </summary>
public class exitButton : MonoBehaviour {

	private Button exit;
	private GameObject playerScore;
	private Scores score;

	/// <summary>
	/// Start this instance.
	/// </summary>
	void Start () {

		exit = (Button)GameObject.Find ("exitButton").GetComponent<Button> ();
		exit.onClick.AddListener(() => exitClicked());

		playerScore = GameObject.Find ("PlayerScore");
		score = playerScore.GetComponent<Scores> ();
	}

	/// <summary>
	/// Function for when exit button is clicked.
	/// Saves the current score for the player.
	/// </summary>
	public void exitClicked () {

		score.SaveScore ();
		Application.Quit ();
	}
}
