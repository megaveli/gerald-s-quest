﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Pause game script. Opens the pause menu.
/// </summary>
public class PauseGame : MonoBehaviour {

	public Transform canvas;

	/// <summary>
	/// Update this instance.
	/// </summary>
	void Update () {
		if (Input.GetKeyDown(KeyCode.Escape)) 
		{
			Pause ();
		}
	}

	/// <summary>
	/// Exit this instance when Exit is clicked.
	/// </summary>
	public void Exit () {

		Debug.Log ("lul");
		Application.Quit ();
	}

	/// <summary>
	/// Pause this game and opens the pause menu.
	/// </summary>
	public void Pause() 
	{
		if (canvas.gameObject.activeInHierarchy == false) 
			{			
				canvas.gameObject.SetActive(true);
				Time.timeScale = 0;
			} 
		else 
			{
				canvas.gameObject.SetActive(false);
				Time.timeScale = 1;
		}
	}
}
