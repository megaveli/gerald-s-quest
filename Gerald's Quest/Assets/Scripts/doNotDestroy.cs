﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Do not destroy. Used for the Gameobjects we wanted to use in every scene in the game
/// </summary>
public class doNotDestroy : MonoBehaviour {

	/// <summary>
	/// Awake this instance.
	/// </summary>
	void Awake () {
		DontDestroyOnLoad (transform.gameObject);
	}
}