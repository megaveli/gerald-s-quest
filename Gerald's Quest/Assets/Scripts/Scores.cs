﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Score tracking script. Holds the information in both the instance variables as well
/// as in text boxes inside a panel. Can also save the scores locally.
/// </summary>
public class Scores : MonoBehaviour {


	private int score;
	private int lowScore;
	private int lowScore2;
	private int lowScore3;

	private string prefName;
	private string lowName;
	private string lowName2;
	private string lowName3;

	private string lowScoreKey = "LowScore";
	private string lowScoreKey2 = "2nd";
	private string lowScoreKey3 = "3rd";

	private string lowNameKey = "LowScoreName";
	private string lowNameKey2 = "LowScoreName2";
	private string lowNameKey3 = "LowScoreName3";
	private string prefNameKey = "PlayerName";

	private List<int> lowScores = new List<int> (3);

	private GameObject scorePanel;

	private Text scoreText;
	private Text lowScoreText;
	private Text lowScoreText2;
	private Text lowScoreText3;

	/// <summary>
	/// Start this instance.
	/// </summary>
	// Use this for initialization
	void Start () {

		/*PlayerPrefs.SetInt (lowScoreKey, 300);
		PlayerPrefs.SetString (lowNameKey, "Geraldina");
		PlayerPrefs.SetInt (lowScoreKey2, 400);
		PlayerPrefs.SetString (lowNameKey2, "Walter");
		PlayerPrefs.SetInt (lowScoreKey3, 500);
		PlayerPrefs.SetString (lowNameKey3, "Geraldo");*/
		scorePanel = GameObject.Find ("scorePanel");

		scoreText = (Text)GameObject.Find ("scoreText").GetComponent<Text>();
		lowScoreText = (Text)GameObject.Find ("lowScore").GetComponent<Text>();
		lowScoreText2 = (Text)GameObject.Find ("lowScore2").GetComponent<Text>();
		lowScoreText3 = (Text)GameObject.Find ("lowScore3").GetComponent<Text>();

		this.score = 0;

		lowScore = PlayerPrefs.GetInt(lowScoreKey,300); 
		lowScore2 = PlayerPrefs.GetInt(lowScoreKey2,400);
		lowScore3 = PlayerPrefs.GetInt(lowScoreKey3,500);

		lowName = PlayerPrefs.GetString (lowNameKey, "Geraldina");
		lowName2 = PlayerPrefs.GetString (lowNameKey2, "Walter");
		lowName3 = PlayerPrefs.GetString (lowNameKey3, "Geraldo");

		prefName = PlayerPrefs.GetString (prefNameKey, "Default");

		scorePanel.SetActive (false);

	}
	/// <summary>
	/// Changes the score.
	/// </summary>
	/// <param name="amount">Amount.</param>
	public void ChangeScore (int amount)
	{
		if (this.score + amount > 0) {
			this.score += amount;
		}else {
			Debug.Log ("The score would be lower than 0");
		}

	}
	/// <summary>
	/// Sets the score.
	/// </summary>
	/// <param name="amount">Amount.</param>
	public void SetScore (int amount)
	{
		if (amount > 0) {
			this.score = amount;
		} else {
			Debug.Log ("The score would be lower than 0");
		}
	}

	/// <summary>
	/// Gets the score.
	/// </summary>
	public void GetScore()
	{
		this.scoreText.text = "Your score is " + score + " Your name: " + prefName; 
		this.lowScoreText.text = "The lowscore is " + lowScore + " points " + "  By: " + lowName;
		this.lowScoreText2.text = "2nd " + lowScore2 + " points " + "  By: " + lowName2;
		this.lowScoreText3.text = "3rd " + lowScore3 + " points " + "  By: " + lowName3;

	}

	/// <summary>
	/// Saves the score.
	/// </summary>
	public void SaveScore ()
	{
		if (score < lowScore3 && score > lowScore2) {
			PlayerPrefs.SetInt (lowScoreKey3, score);
			PlayerPrefs.SetString (lowNameKey3, prefName);
			PlayerPrefs.Save ();
		} else if (score < lowScore2 && score > lowScore) {
			PlayerPrefs.SetInt (lowScoreKey2, score);
			PlayerPrefs.SetString (lowNameKey2, prefName);
			PlayerPrefs.Save ();
		} else if (score < lowScore) {
			PlayerPrefs.SetInt (lowScoreKey, score);
			PlayerPrefs.SetString (lowNameKey, prefName);
			PlayerPrefs.Save ();
		}
	}

	/// <summary>
	/// Shows the score.
	/// </summary>
	public void ShowScore ()
	{
		scorePanel.SetActive (true);
	}

	/// <summary>
	/// Gets the player score.
	/// </summary>
	/// <returns>The player score.</returns>
	public int GetPlayerScore ()
	{
		return this.score;
	}

	/// <summary>
	/// Update this instance.
	/// </summary>
	// Update is called once per frame
	void Update () {
		GetScore ();

	}
		
}
