﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/// <summary>
/// Name class. Used in the EnterName scene to save the players name
/// Then with the button move to next scene
/// </summary>
public class Name : MonoBehaviour {


	private Text nameText;

	private Button continueButton;

	private string prefName;
	private string prefNameKey = "PlayerName";




	/// <summary>
	/// Start this instance.
	/// </summary>
	void Start() {
		nameText = (Text)GameObject.Find("nameText").GetComponent<Text>();

		continueButton = (Button)GameObject.Find ("continueButton").GetComponent<Button> ();

		continueButton.onClick.AddListener (() => ContinueClicked ());
	}

	/// <summary>
	/// For the player to choose their name.
	/// </summary>
	public void EnterName()
	{
		foreach (char c in Input.inputString) {
			if (c == "\b" [0]) {
				if (nameText.text.Length != 0) {
					nameText.text = nameText.text.Substring (0, nameText.text.Length - 1);
					/*	} else if (c == "\n" [0] || c == "\r" [0] || c == "\r\n" [0]) {
					name1.text = nameText.text; */
				} else
					nameText.text += c;
			}
		}

	}

	/// <summary>
	///  When continues is clicked.
	/// </summary>
	public void ContinueClicked()
	{
		prefName = nameText.text.ToString();
		Debug.Log (prefName);
		PlayerPrefs.SetString (prefNameKey, prefName);
		SceneManager.LoadScene ("GeraldsRoomNew");

	}

	/// <summary>
	/// Gets the name that will be saved.
	/// </summary>
	/// <returns>The name.</returns>
	public string GetName()
	{
		return this.prefName;
	}

	/// <summary>
	/// Update this instance.
	/// </summary>
	void Update()
	{
		EnterName ();
	}



}
