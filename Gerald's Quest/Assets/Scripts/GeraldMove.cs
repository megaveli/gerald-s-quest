using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Gerald move. Script for movement and animation of the main character
/// </summary>
public class GeraldMove : MonoBehaviour {

	private float speed;
	private bool canMove = true;
	private float _test;


	private float _targetX;
	private bool _moving;													
	private Animator animator;


	/// <summary>
	/// Start this instance.
	/// </summary>
	void Start(){

		speed = 3;
		_test = 0.5f;
		animator = this.GetComponent<Animator>();
	}


	/// <summary>
	/// Checks if mouse is down. When it is down and the canMove is true, changes the _targetX
	/// and moves the object this scipt is on to the target
	/// </summary>
	public void Move (){
		

		if (Input.GetMouseButtonDown (0)) {							
			

			if (canMove) {
				if (Input.GetMouseButtonDown (0)) {
					
					_targetX = Camera.main.ScreenToWorldPoint (Input.mousePosition).x;
					_moving = true;
					if (_targetX < transform.position.x) {
						_targetX += _test;
					} else if (_targetX > transform.position.x) {
						_targetX -= _test;
					}
				}
			}
		} 


		if (_moving) {
			if (Mathf.Abs(_targetX - transform.position.x) >= speed * Time.deltaTime)
				transform.position = Vector3.MoveTowards(transform.position, new Vector3(_targetX, transform.position.y), Time.deltaTime * speed);
			else {
				transform.position = new Vector3(_targetX, transform.position.y, transform.position.z);
				_moving = false;
			}
		}
	}


	/// <summary>
	/// Animate this instance. Gives the Gameobject this script is on animations
	/// based on the direction they are moving to.
	/// </summary>
	public void Animate(){

		if (_moving) {


			if (_targetX < transform.position.x) {
				animator.SetInteger ("Direction", 1);	
			} else if (_targetX > transform.position.x) {
				animator.SetInteger ("Direction", 2);
			}
		}

		else {
			animator.SetInteger ("Direction", 0);
		}
	}

	/// <summary>
	/// Changes the canMove boolean. Can be used from other scripts.
	/// </summary>
	public void ChangeMove()
	{
		if (canMove == true) {
			canMove = false;
		} else {
			canMove = true;
		}
	}

	/// <summary>
	/// Update this instance.
	/// </summary>
	void Update() {

	
		Move ();
		Animate ();

		}
}
