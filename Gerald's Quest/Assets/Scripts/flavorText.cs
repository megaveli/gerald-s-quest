﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Flavor text. Opens a panel that contains the flavor text for items in inventory
/// </summary>
public class flavorText : MonoBehaviour {


	private Text flavor;
	private GameObject InventorySystem;
	private InventorySystem inventory;
	private GameObject flavorPanel;


	/// <summary>
	/// Start this instance.
	/// </summary>
	// Use this for initialization
	void Start () {

		flavor = (Text)GameObject.Find ("flavorText").GetComponent<Text> ();
		InventorySystem = GameObject.Find ("InventorySystem");
		inventory = InventorySystem.GetComponent<InventorySystem> ();
		flavorPanel = GameObject.Find ("flavorPanel");

		flavorPanel.SetActive (false);
	}

	/// <summary>
	/// Raises the mouse over0 event.
	/// Shows the flavor text of the item in the first inventory slot
	/// </summary>
	public void OnMouseOver0()
	{
		flavorPanel.SetActive (true);
		if (inventory.GetInventoryCount () >= 1) {
			flavor.text = inventory.GetFlavor (0);
		} else {
			flavorPanel.SetActive (false);
		}
	}

	/// <summary>
	/// Raises the mouse over1 event.
	/// Shows the flavor text of the item in the second inventory slot
	/// </summary>
	public void OnMouseOver1()
	{
		flavorPanel.SetActive (true);
		if (inventory.GetInventoryCount () >= 2) {
			flavor.text = inventory.GetFlavor (1);
		} else {
			flavorPanel.SetActive (false);
		}
	}

	/// <summary>
	/// Raises the mouse over2 event.
	/// Shows the flavor text of the item in the third inventory slot
	/// </summary>
	public void OnMouseOver2()
	{
		flavorPanel.SetActive (true);
		if (inventory.GetInventoryCount () == 3) {
			flavor.text = inventory.GetFlavor (2);
		} else {
			flavorPanel.SetActive (false);
		}
	}

	/// <summary>
	/// Raises the mouse exit event.
	/// Hides the flavor text panel when cursor is no longer on top of the buttons
	/// </summary>
	public void OnMouseExit()
	{
		flavorPanel.SetActive (false);
	}
	/// <summary>
	/// Update this instance.
	/// </summary>
	// Update is called once per frame
	void Update () {
		
	}
}
