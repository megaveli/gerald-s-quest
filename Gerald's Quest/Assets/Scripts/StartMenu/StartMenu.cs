﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class StartMenu : MonoBehaviour {

	private Button startButton;
	private Button optionsButton;
	private Button exitButton;
	private Button storyButton;
	private RawImage optionsButtonBroken;

	void Start () {

		optionsButtonBroken = GameObject.Find ("optionsButtonBroken").GetComponent<RawImage> ();

		startButton = (Button)GameObject.Find ("startButton").GetComponent<Button> ();
		optionsButton = (Button)GameObject.Find ("optionsButton").GetComponent<Button> ();
		exitButton = (Button)GameObject.Find ("exitButton").GetComponent<Button> ();
		storyButton = (Button)GameObject.Find ("storyButton").GetComponent<Button> ();

		startButton.onClick.AddListener(() => MenuButtonClicked(startButton));
		optionsButton.onClick.AddListener (() => MenuButtonClicked (optionsButton));
		exitButton.onClick.AddListener (() => MenuButtonClicked (exitButton));
		storyButton.onClick.AddListener (() => MenuButtonClicked (storyButton));
		
	}
	/// <summary>
	/// Checks which button was pressed and runs it.
	/// </summary>
	/// <param name="menuButton">Menu button.</param>
	void MenuButtonClicked(Button menuButton){

		if (menuButton == startButton) {

			SceneManager.LoadScene ("EnterName");

		} else if (menuButton == exitButton) {

			Application.Quit ();

		} else if (menuButton == optionsButton) {

			menuButton.transform.localScale = new Vector3 (0, 0, 0);
			optionsButtonBroken.transform.localScale = new Vector3 (0.1f, 0.1f, 1);

		} else if (menuButton == storyButton) {

			SceneManager.LoadScene ("Story");
			
		}
	}
	

	void Update () {
		
	}
}
