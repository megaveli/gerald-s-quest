﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour {

	private int confidence;
	private float multiplier;
	private int maxValue;
	private int minValue;

	private Image confFull;
	private Image confEmpty;

	private Text confLevel;

	private GameObject playerScore;

	private Scores score;

	void Start () {
		this.confidence = 10;
		this.maxValue = 100;
		this.minValue = 0;

		playerScore = GameObject.Find ("PlayerScore");
		score = playerScore.GetComponent<Scores> ();

		confFull = (Image)GameObject.Find ("confFull").GetComponent<Image> ();
		confEmpty = (Image)GameObject.Find ("confEmpty").GetComponent<Image> ();

		confLevel = (Text)GameObject.Find ("confLevel").GetComponent<Text> ();
	}
	/// <summary>
	/// Sets the confidence.
	/// Doesn't allow it to be higher than max value or lower than min value.
	/// </summary>
	/// <param name="conf">Conf.</param>
	public void SetConf(int conf) {

		int score = conf - this.confidence;


		if (conf > maxValue) {
			this.confidence = maxValue;
		} else if (conf < minValue) {
			this.confidence = minValue;
		} else {
			this.confidence = conf;
		}

		this.score.ChangeScore (score);
	}
	/// <summary>
	/// Changes the confidence.
	/// Doesn't allow it to be higher than max value or lower than min value.
	/// </summary>
	/// <param name="conf">Conf.</param>
	public void ChangeConf(int conf) {

		if (this.confidence + conf > maxValue) {
			this.confidence = maxValue;
		} else if (this.confidence + conf < minValue) {
			this.confidence = minValue;
		} else {
			this.confidence += conf;
		}

		this.score.ChangeScore (conf);

	}
	/// <summary>
	/// Gets the confidence.
	/// </summary>
	/// <returns>The conf.</returns>
	public float GetConf () {
		return this.confidence;
	}
	/// <summary>
	/// Gets the multiplier.
	/// </summary>
	public void GetMultiplier () {
		this.multiplier = (float)GetConf () / 100;
	}


	public void StopConfidence()
	{
		confFull.transform.localScale = new Vector3 (0, 0, 0);
		confEmpty.transform.localScale = new Vector3 (0, 0, 0);
		confLevel.transform.localScale = new Vector3 (0, 0, 0);
	}

	public void GetConfLevel()
	{
		confLevel.text = " " + GetConf () + "%";
	}

	public void RemoveConfLevel()
	{
		confLevel.text = " ";
	}


	/// <summary>
	/// Method for confidence bar on UI.
	/// </summary>
	public void ConfidenceBar () {
		GetMultiplier ();
		confFull.fillAmount = multiplier;
	}
		
}
