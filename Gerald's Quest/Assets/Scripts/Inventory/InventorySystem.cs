﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/// <summary>
/// Inventory system. Specifies what each item button will do in the game.
/// Creates a list for inventory with InventoryList class.
/// Has images for each inventory slot that update constantly when items are used and picked up.
/// </summary>
public class InventorySystem : MonoBehaviour {

	private Scene scene;

	private Button appleButton;
	private Button bananaButton;
	private Button iosButton;
	private Button beerButton;
	private Button coffeeButton;
	private Button cologneButton;
	private Button towelButton;
	private Button luckyButton;
	private Button chargerButton;

	private Button slot1;
	private Button slot2;
	private Button slot3;

	private RawImage slot1Img;
	private RawImage slot2Img;
	private RawImage slot3Img;

	private Button inventoryButton;
	private GameObject inventoryPanel;
	private InventoryList inventory;
	private Dictionary<Button,Item> itemMap;

	private GameObject playerScore;

	private Player player;

	/// <summary>
	/// Start this instance.
	/// </summary>
	// Use this for initialization
	void Start () {


		playerScore = GameObject.Find ("PlayerScore");
		player = playerScore.GetComponent<Player> ();

		itemMap = new Dictionary<Button,Item> ();
		
		appleButton = (Button)GameObject.Find ("apple").GetComponent<Button> ();
		bananaButton = (Button)GameObject.Find ("banana").GetComponent<Button> ();
		iosButton = (Button)GameObject.Find ("ios").GetComponent<Button> ();
		beerButton = (Button)GameObject.Find ("beer").GetComponent<Button> ();
		coffeeButton = (Button)GameObject.Find ("coffee").GetComponent<Button> ();
		cologneButton = (Button)GameObject.Find ("cologne").GetComponent<Button> ();
		towelButton = (Button)GameObject.Find ("towel").GetComponent<Button> ();
		luckyButton = (Button)GameObject.Find ("luckyHat").GetComponent<Button> ();
		chargerButton = (Button)GameObject.Find ("charger").GetComponent<Button> ();

		itemMap.Add (appleButton, new Item ("apple_final", 10, "apple_final", "It's an apple"));
		itemMap.Add (bananaButton, new Item ("banana_final", 5, "banana_final", "It's in perfect shape!"));
		itemMap.Add (iosButton, new Item ("phone_final", 15, "phone_final", "iPhone 8" +"\n" + "Only one contact, Gertrude"));
		itemMap.Add (beerButton, new Item ("beer_final", 50, "beer_final", "Without fun, there can be beer"));
		itemMap.Add (coffeeButton, new Item ("coffee_final", 20, "coffee_final", "Can't live without this"));
		itemMap.Add (cologneButton, new Item ("cologne_final", 5, "cologne_final", "Unholy water"));
		itemMap.Add (towelButton, new Item ("towel_final", 25, "towel_final", "Don't panic!"));
		itemMap.Add (luckyButton, new Item ("luckyHat", 0, "luckyHat", "You feel lucky with this on." + "\n" + "You don't want to lose this."));
		itemMap.Add (chargerButton, new Item ("charger_final", 10, "charger_final", "Half the equipment you need for your phone"));

		slot1 = (Button)GameObject.Find ("slot1").GetComponent<Button> ();
		slot2 = (Button)GameObject.Find ("slot2").GetComponent<Button> ();
		slot3 = (Button)GameObject.Find ("slot3").GetComponent<Button> ();

		slot1Img = (RawImage)GameObject.Find ("slot1").GetComponent<RawImage> ();
		slot2Img = (RawImage)GameObject.Find ("slot2").GetComponent<RawImage> ();
		slot3Img = (RawImage)GameObject.Find ("slot3").GetComponent<RawImage> ();

		inventoryButton = (Button)GameObject.Find ("inventoryButton").GetComponent<Button> ();

		inventory = new InventoryList ();

		inventoryPanel = GameObject.Find ("inventoryPanel");

		appleButton.onClick.AddListener (()=> ItemClicked(appleButton));
		bananaButton.onClick.AddListener (() => ItemClicked (bananaButton));
		iosButton.onClick.AddListener (() => ItemClicked (iosButton));
		beerButton.onClick.AddListener (() => ItemClicked (beerButton));
		coffeeButton.onClick.AddListener (() => ItemClicked (coffeeButton));
		cologneButton.onClick.AddListener (() => ItemClicked (cologneButton));
		towelButton.onClick.AddListener (() => ItemClicked (towelButton));
		luckyButton.onClick.AddListener (() => ItemClicked (luckyButton));
		chargerButton.onClick.AddListener (() => ItemClicked (chargerButton));

		slot1.onClick.AddListener (() => SlotClicked (slot1));
		slot2.onClick.AddListener (() => SlotClicked (slot2));
		slot3.onClick.AddListener (() => SlotClicked (slot3));

		inventoryButton.onClick.AddListener (() => PanelClicked (inventoryButton));

		inventoryPanel.SetActive (false);

		RandomItems ();

	}
		
		
	/// <summary>
	/// Changes whether the inventory panel is active or not
	/// </summary>
	/// <param name="Bo">Bo.</param>
	public void PanelClicked(Button Bo) {

		Debug.Log (Bo);
		inventoryPanel.SetActive (!inventoryPanel.activeSelf);
	}

	/// <summary>
	/// Defines what happens when items are clicked in the game world.
	/// Adds them to the inventory and hides the button in the game.
	/// </summary>
	/// <param name="item"> Button to represent an item</param>
	public void ItemClicked(Button item){

		Debug.Log (item);
		if (this.inventory.GetCount () < 3) {
			this.inventory.AddItem (itemMap [item]);
			Debug.Log (this.inventory.GetInventory ());
			item.transform.localScale = new Vector3 (0, 0, 0);
		}
	}

	/// <summary>
	/// Defines what happens when inventory slots are clicked.
	/// Uses the UseItem() Method and then removes them from the inventory
	/// with the RemoveItem() Method. Unless the item is Lucky Hat
	/// </summary>
	/// <param name="slot">Slot in the inventory between 0 and 2.</param>
	public void SlotClicked(Button slot){
		Debug.Log (slot);
		if (inventory.GetCount () != 0) {
			if (slot == slot1) {
				if (inventory.GetFile (0) != "luckyHat") {
					UseItem (0);
					inventory.RemoveItem (0);
				}
			} else if (slot == slot2 && inventory.GetCount() > 1) {
				if (inventory.GetFile (1) != "luckyHat") {
					UseItem (1);
					inventory.RemoveItem (1);
				}
			} else if (slot == slot3 && inventory.GetCount() == 3) {
				if (inventory.GetFile (2) != "luckyHat") {
					UseItem (2);
					inventory.RemoveItem (2);
				}
			}
		}
	}

	/// <summary>
	/// Uses the item in the inventory. The player gains confidence.
	/// </summary>
	/// <param name="index">Index.</param>
	public void UseItem(int index){
		player.ChangeConf (this.inventory.GetConf (index));

		Debug.Log (this.inventory.GetConf (index));
	}
		
	/// <summary>
	/// Updates the images in the inventory slots. If they are empty, they get image to
	/// represent that.
	/// </summary>
	public void UpdateImages() {
		if (inventory.GetCount () == 1) {
			slot1Img.texture = (Texture)Resources.Load (this.inventory.GetFile (0));
			slot1Img.SetNativeSize ();
			slot1Img.transform.localScale = new Vector3 (1f, 1f, 1f);

			slot2Img.texture = (Texture)Resources.Load ("no");
			slot2Img.SetNativeSize ();
			slot2Img.transform.localScale = new Vector3 (1f, 1f, 1f);

			slot3Img.texture = (Texture)Resources.Load ("no");
			slot3Img.SetNativeSize ();
			slot3Img.transform.localScale = new Vector3 (1f, 1f, 1f);

		} else if (inventory.GetCount () == 2) {
			slot1Img.texture = (Texture)Resources.Load (this.inventory.GetFile (0));
			slot1Img.SetNativeSize ();
			slot1Img.transform.localScale = new Vector3 (1f, 1f, 1f);

			slot2Img.texture = (Texture)Resources.Load (this.inventory.GetFile (1));
			slot2Img.SetNativeSize ();
			slot2Img.transform.localScale = new Vector3 (1f, 1f, 1f);

			slot3Img.texture = (Texture)Resources.Load ("no");
			slot3Img.SetNativeSize ();
			slot3Img.transform.localScale = new Vector3 (1f, 1f, 1f);

		} else if (inventory.GetCount () == 3) {
			slot1Img.texture = (Texture)Resources.Load (this.inventory.GetFile (0));
			slot1Img.SetNativeSize ();
			slot1Img.transform.localScale = new Vector3 (1f, 1f, 1f);

			slot2Img.texture = (Texture)Resources.Load (this.inventory.GetFile (1));
			slot2Img.SetNativeSize ();
			slot2Img.transform.localScale = new Vector3 (1f, 1f, 1f);

			slot3Img.texture = (Texture)Resources.Load (this.inventory.GetFile (2));
			slot3Img.SetNativeSize ();
			slot3Img.transform.localScale = new Vector3 (1f, 1f, 1f);

		} else if (inventory.GetCount () == 0) {
			slot1Img.texture = (Texture)Resources.Load ("no");
			slot1Img.SetNativeSize ();
			slot1Img.transform.localScale = new Vector3 (1f, 1f, 1f);

			slot2Img.texture = (Texture)Resources.Load ("no");
			slot2Img.SetNativeSize ();
			slot2Img.transform.localScale = new Vector3 (1f, 1f, 1f);

			slot3Img.texture = (Texture)Resources.Load ("no");
			slot3Img.SetNativeSize ();
			slot3Img.transform.localScale = new Vector3 (1f, 1, 1f);
		}
	}

	/// <summary>
	/// Gets the flavor text from the items in the inventory.
	/// </summary>
	/// <returns>The flavor.</returns>
	/// <param name="index">Index in the inventory, between 0 and 2.</param>
	public string GetFlavor (int index)
	{

		return this.inventory.GetFlavorText (index);

	}

	/// <summary>
	/// Gets the inventory count.
	/// </summary>
	/// <returns>The inventory count.</returns>
	public int GetInventoryCount ()
	{
		return this.inventory.GetCount ();
	}

	/// <summary>
	/// Update this instance.
	/// </summary>
	// Update is called once per frame
	void Update () {
		UpdateImages ();
		player.ConfidenceBar ();
		scene = SceneManager.GetActiveScene ();


		if (scene.name == "BackToSchoolhall" || scene.name == "BackToHome"
			|| scene.name == "BackAtHome" || scene.name == "BackToBed" || scene.name == "Credits") {
			player.StopConfidence ();
			inventoryButton.transform.localScale = new Vector3 (0, 0, 0);
			inventoryPanel.SetActive (false);
		}
	}

	/// <summary>
	/// Randomize the items that are in the game.
	/// </summary>

	public void RandomItems()
	{

		// generate 4 random numbers
		int rng1 = Random.Range (1, 10);

		int rng2 = Random.Range (1, 10);
		while (rng2 == rng1) {
			rng2 = Random.Range (1, 10);
			}

		int rng3 = Random.Range (1, 10);
		while (rng3 == rng1 || rng3 == rng2) {
			rng3 = Random.Range (1, 10);
			}

		int rng4 = Random.Range (1, 10);
		while (rng4 == rng1 || rng4 == rng2 || rng4 == rng3) {
			rng4 = Random.Range (1, 10);
			}

			

		// Each random number will "hide" one button
		if (rng1 == 1) {							
			appleButton.transform.localScale = new Vector3 (0, 0, 0);
		} else if (rng1 == 2) {
			bananaButton.transform.localScale = new Vector3 (0, 0, 0);
		} else if (rng1 == 3) {
			iosButton.transform.localScale = new Vector3 (0, 0, 0);
		} else if (rng1 == 4) {
			beerButton.transform.localScale = new Vector3 (0, 0, 0);
		} else if (rng1 == 5) {
			coffeeButton.transform.localScale = new Vector3 (0, 0, 0);
		} else if (rng1 == 6) {
			cologneButton.transform.localScale = new Vector3 (0, 0, 0);
		} else if (rng1 == 7) {
			towelButton.transform.localScale = new Vector3 (0, 0, 0);
		} else if (rng1 == 8) {
			luckyButton.transform.localScale = new Vector3 (0, 0, 0);
		} else if (rng1 == 9) {
			chargerButton.transform.localScale = new Vector3 (0, 0, 0);
		}

		if (rng2 == 1) {							
			appleButton.transform.localScale = new Vector3 (0, 0, 0);
		} else if (rng2 == 2) {
			bananaButton.transform.localScale = new Vector3 (0, 0, 0);
		} else if (rng2 == 3) {
			iosButton.transform.localScale = new Vector3 (0, 0, 0);
		} else if (rng2 == 4) {
			beerButton.transform.localScale = new Vector3 (0, 0, 0);
		} else if (rng2 == 5) {
			coffeeButton.transform.localScale = new Vector3 (0, 0, 0);
		} else if (rng2 == 6) {
			cologneButton.transform.localScale = new Vector3 (0, 0, 0);
		} else if (rng2 == 7) {
			towelButton.transform.localScale = new Vector3 (0, 0, 0);
		} else if (rng2 == 8) {
			luckyButton.transform.localScale = new Vector3 (0, 0, 0);
		} else if (rng2 == 9) {
			chargerButton.transform.localScale = new Vector3 (0, 0, 0);
		}

		if (rng3 == 1) {							
			appleButton.transform.localScale = new Vector3 (0, 0, 0);
		} else if (rng3 == 2) {
			bananaButton.transform.localScale = new Vector3 (0, 0, 0);
		} else if (rng3 == 3) {
			iosButton.transform.localScale = new Vector3 (0, 0, 0);
		} else if (rng3 == 4) {
			beerButton.transform.localScale = new Vector3 (0, 0, 0);
		} else if (rng3 == 5) {
			coffeeButton.transform.localScale = new Vector3 (0, 0, 0);
		} else if (rng3 == 6) {
			cologneButton.transform.localScale = new Vector3 (0, 0, 0);
		} else if (rng3 == 7) {
			towelButton.transform.localScale = new Vector3 (0, 0, 0);
		} else if (rng3 == 8) {
			luckyButton.transform.localScale = new Vector3 (0, 0, 0);
		} else if (rng3 == 9) {
			chargerButton.transform.localScale = new Vector3 (0, 0, 0);
		}

		if (rng4 == 1) {							
			appleButton.transform.localScale = new Vector3 (0, 0, 0);
		} else if (rng4 == 2) {
			bananaButton.transform.localScale = new Vector3 (0, 0, 0);
		} else if (rng4 == 3) {
			iosButton.transform.localScale = new Vector3 (0, 0, 0);
		} else if (rng4 == 4) {
			beerButton.transform.localScale = new Vector3 (0, 0, 0);
		} else if (rng4 == 5) {
			coffeeButton.transform.localScale = new Vector3 (0, 0, 0);
		} else if (rng4 == 6) {
			cologneButton.transform.localScale = new Vector3 (0, 0, 0);
		} else if (rng4 == 7) {
			towelButton.transform.localScale = new Vector3 (0, 0, 0);
		} else if (rng4 == 8) {
			luckyButton.transform.localScale = new Vector3 (0, 0, 0);
		} else if (rng4 == 9) {
			chargerButton.transform.localScale = new Vector3 (0, 0, 0);
		}

	}
}
