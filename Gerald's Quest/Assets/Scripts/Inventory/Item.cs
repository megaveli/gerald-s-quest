﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Item class. Each item in the game will have a name, confidence amount, filename and flavor text
/// </summary>
public class Item {

	private string name;
	private int conf;
	private string fileName;
	private string flavor;

	/// <summary>
	/// Initializes a new instance of the <see cref="Item"/> class.
	/// </summary>
	/// <param name="name">Name of the item.</param>
	/// <param name="confidence">Confidence gained from the item.</param>
	/// <param name="file">File name of the item.</param>
	/// <param name="flavor">Flavor text of the item.</param>
	public Item (string name, int confidence, string file, string flavor) {

		this.name = name;
		this.conf = confidence;
		this.fileName = file;
		this.flavor = flavor;
	}

	/// <summary>
	/// Gets the name of the item.
	/// </summary>
	/// <returns>The name.</returns>
	public string GetName(){

		return this.name;
	}

	/// <summary>
	/// Gets the file name of the item.
	/// </summary>
	/// <returns>The file name.</returns>
	public string GetFileName()
	{
		return this.fileName;
	}

	/// <summary>
	/// Gets the confidence of the item.
	/// </summary>
	/// <returns>The confidence.</returns>
	public int GetConfidence()
	{
		return this.conf;
	}

	/// <summary>
	/// Gets the flavortext of the item.
	/// </summary>
	/// <returns>The flavor.</returns>
	public string GetFlavor()
	{
		return this.flavor;
	}
		
}
