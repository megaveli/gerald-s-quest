﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Inventory list. Represents the inventory in the form of a list. 
/// Used to hold Item objects
/// </summary>
public class InventoryList : MonoBehaviour {

	private List<Item> inventory;

	/// <summary>
	/// Initializes a new instance of the <see cref="InventoryList"/> class.
	/// </summary>
	public InventoryList(){
		this.inventory = new List<Item> ();
	}

	/// <summary>
	/// Gets the inventory.
	/// </summary>
	/// <returns>The inventory.</returns>
	public string GetInventory(){
		string thong = "";
		foreach (Item item in this.inventory) {
			thong += item.GetName () + "\n";
		}
		return thong;
	}

	/// <summary>
	/// Adds the item to the list.
	/// </summary>
	/// <param name="item">Item.</param>
	public void AddItem(Item item){
		this.inventory.Add (item);
	}

	/// <summary>
	/// Removes the item from the list.
	/// </summary>
	/// <param name="index">Index.</param>
	public void RemoveItem(int index){
		this.inventory.RemoveAt (index);
	}

	/// <summary>
	/// Counts how many items are in the inventory
	/// </summary>
	/// <returns>The count.</returns>
	public int GetCount(){
		return this.inventory.Count;
	}

	/// <summary>
	/// Gets the file name of the items in inventory in specific slot.
	/// </summary>
	/// <returns>The file.</returns>
	/// <param name="index">Index.</param>
	public string GetFile(int index){
		return this.inventory [index].GetFileName ();
	}

	/// <summary>
	/// Gets the confidence of the items in inventory in specific slot.
	/// </summary>
	/// <returns>The conf.</returns>
	/// <param name="index">Index.</param>
	public int GetConf(int index){
		return this.inventory [index].GetConfidence ();
	}

	/// <summary>
	/// Gets the flavor text of the items in inventory in specific slot.
	/// </summary>
	/// <returns>The flavor text.</returns>
	/// <param name="index">Index.</param>
	public string GetFlavorText (int index) {
		return this.inventory [index].GetFlavor ();
	}
}
