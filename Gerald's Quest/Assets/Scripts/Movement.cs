﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class named Movement. Used in Gameobject to find Gerald in each of the scenes
/// To be able to choose whether he can move or not
/// </summary>
public class Movement : MonoBehaviour {

	private GameObject gerald;
	private GeraldMove move;

	/// <summary>
	/// Start this instance.
	/// </summary>
	// Use this for initialization
	void Start () {

	}

	/// <summary>
	/// Changes the canMove boolean in the GeraldMove script.
	/// </summary>
	public void ChangeMove()
	{
		move.ChangeMove ();
	}

	/// <summary>
	/// Update this instance.
	/// </summary>
	// Update is called once per frame
	void Update () {
		gerald = GameObject.Find ("Gerald2");
		move = gerald.GetComponent<GeraldMove> ();
	}
}
