﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class seeYa : MonoBehaviour {

	private Canvas oiVas;
	private RawImage getOverHere;
	private Text oi;
	private BoxCollider seeYaa;


	void Start () {

		oiVas = (Canvas)GameObject.Find ("oiVas").GetComponent<Canvas> ();
		seeYaa = (BoxCollider)GameObject.Find ("seeYaa").GetComponent<BoxCollider> ();

		oiVas.enabled = false;

	}
	/// <summary>
	/// shows canvas	
	/// </summary>
	/// <param name="batman">Batman.</param>
	private void OnTriggerEnter (Collider batman) {
		if (batman.tag == "gerald") {
			oiVas.enabled = true;
		}
	}
	/// <summary>
	/// hides canvas and destroys collider
	/// </summary>
	/// <param name="batman">Batman.</param>
	private void OnTriggerExit (Collider batman) {
		if (batman.tag == "gerald") {
			oiVas.enabled = false;
			Destroy (seeYaa);
		}
	}
}
