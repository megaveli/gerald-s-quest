﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ReflectingTime : MonoBehaviour {

	private GameObject playerScore;
	private Player player;
	private Canvas geraldVas;
	private RawImage geraldBubble;
	private Text geraldJap;
	private float check;

	void Start () {

		playerScore = GameObject.Find ("PlayerScore");
		player = playerScore.GetComponent<Player> ();

		geraldVas = (Canvas)GameObject.Find ("geraldVas").GetComponent<Canvas> ();
		geraldJap = (Text)GameObject.Find ("geraldJap").GetComponent<Text> ();

		geraldVas.enabled = false;
		check = player.GetConf ();

	}
	/// <summary>
	/// entering collider shows text depending on confidence ammount
	/// </summary>
	/// <param name="batman">Batman.</param>
	private void OnTriggerEnter (Collider batman) {
		if (batman.tag == "gerald") {
			geraldVas.enabled = true;
			if (check >= 25 && check < 50) {
				geraldJap.text = "I can't handle\nthis, but\nI'll try again\ntomorrow.";
			} else if (check >= 50 && check < 75) {
				geraldJap.text = "I think that\nI survived!\nProbably...";
			} else if (check >= 75 && check < 100) {
				geraldJap.text = "Wohoo!!!\nI did it!\nI'm still alive!";
			} else if (check == 100) {
				geraldJap.text = "I'm the best!\nPrrrrrrat!!!\nSuck it God!\n";
			} else {
				geraldJap.text = "I'll never leave home again...";
			}
		}
	}
	/// <summary>
	///exiting collider hides canvas
	/// </summary>
	/// <param name="batman">Batman.</param>
	private void OnTriggerExit (Collider batman) {
		if (batman.tag == "gerald") {
			geraldVas.enabled = false;
		}
	}
}
