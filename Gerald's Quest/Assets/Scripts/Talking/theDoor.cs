﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class theDoor : MonoBehaviour {

	private Canvas inventoryCan;
	private Canvas doorVas;
	private Canvas geraldVas;
	private Button option1;
	private Button option2;
	private Button option3;
	private Button option4;
	private Text geraldJap;
	private Text doorSays;
	private Text ot1;
	private Text ot2;
	private Text ot3;
	private Text ot4;
	private string key;


	void Start () {
		inventoryCan = (Canvas)GameObject.Find ("Canvas").GetComponent<Canvas> ();
		doorVas = (Canvas)GameObject.Find ("doorVas").GetComponent<Canvas> ();
		geraldVas = (Canvas)GameObject.Find ("geraldVas").GetComponent<Canvas> ();

		option1 = (Button)GameObject.Find ("option1").GetComponent<Button> ();
		option2 = (Button)GameObject.Find ("option2").GetComponent<Button> ();
		option3 = (Button)GameObject.Find ("option3").GetComponent<Button> ();
		option4 = (Button)GameObject.Find ("option4").GetComponent<Button> ();

		doorSays = (Text)GameObject.Find ("doorSays").GetComponent<Text> ();
		geraldJap = (Text)GameObject.Find ("geraldJap").GetComponent<Text> ();
		ot1 = (Text)GameObject.Find ("ot1").GetComponent<Text> ();
		ot2 = (Text)GameObject.Find ("ot2").GetComponent<Text> ();
		ot3 = (Text)GameObject.Find ("ot3").GetComponent<Text> ();
		ot4 = (Text)GameObject.Find ("ot4").GetComponent<Text> ();

		option1.onClick.AddListener (() => option1Clicked (option1));
		option2.onClick.AddListener (() => option2Clicked (option2));
		option3.onClick.AddListener (() => option3Clicked (option3));
		option4.onClick.AddListener (() => option4Clicked (option4));

		doorVas.enabled = false;
		geraldVas.enabled = false;


		key = "X";

	}
	/// <summary>
	/// works with right key
	/// </summary>
	/// <param name="one">One.</param>
	private void option1Clicked(Button one){  // Button one, AKA button A //
		// A section //
		if (key == "X") {  
			geraldJap.text = "Umm... I... \n Outside...";
			doorSays.text = "What?\n I can't hear you!";
			ot1.text = "...outside?";
			ot2.text = "...";
			ot3.text = "OUTSIDE!!!";
			ot4.text = "Not my fault";
			key = "A";
		} else if (key == "A") {  
			geraldJap.text = "...outside.";
			doorSays.text = "What about it?";
			ot1.text = "What do you think?";
			ot2.text = "Can't I just go?";
			ot3.text = "Come on...";
			ot4.text = "I'd like to go there";
			key = "AA";
		} else if (key == "AA") {  
			geraldJap.text = "You\nare a door,\nwhat do you think?";
			doorSays.text = "What\ndo we have here?\nDr. Smartypants I presume?\nGo and have a horrible day,\nplease, I insist.  ";
			ot1.text = "GO OUTSIDE";
			option2.transform.localScale = new Vector3 (0, 0, 0);
			option3.transform.localScale = new Vector3 (0, 0, 0);
			option4.transform.localScale = new Vector3 (0, 0, 0);
			key = "AAA";
		} else if (key == "AAA") {  
			SceneManager.LoadScene ("OutsideFinal");
		} else if (key == "AAC") {
			geraldJap.text = "What!? Why!?\nWhy are you\nbeing so weird?";
			doorSays.text = "I... Look... You...\nOh but, look at the time!\nYou must be in a hurry by now!\n...good bye...\n(I'm so lonely...)";
			ot1.text = "GO OUTSIDE";
			option2.transform.localScale = new Vector3 (0, 0, 0);
			option3.transform.localScale = new Vector3 (0, 0, 0);
			option4.transform.localScale = new Vector3 (0, 0, 0);
			key = "AACA";
		} else if (key == "AACA") {
			SceneManager.LoadScene ("OutsideFinal");
		} else if (key == "AB") {
			geraldJap.text = "I am\nnot a fool!\nTake it back!";
			doorSays.text = "Oh,\nyes you are!\nAlso, no takebacksies.";
			ot1.text = "You are a fool!";
			ot2.text = "''Sniff''";
			option3.transform.localScale = new Vector3 (0, 0, 0);
			option4.transform.localScale = new Vector3 (0, 0, 0);
			key = "ABA";
		} else if (key == "ABA") {
			geraldJap.text = "No! You are a fool, fool!";
			doorSays.text = "Wha... You meanie!\nLeave me be.\nJust go, please.";
			ot1.text = "GO OUTSIDE";
			option2.transform.localScale = new Vector3 (0, 0, 0);
			option3.transform.localScale = new Vector3 (0, 0, 0);
			option4.transform.localScale = new Vector3 (0, 0, 0);
			key = "ABAA";
		} else if (key == "ABAA") {
			SceneManager.LoadScene ("OutsideFinal");
		} else if (key == "ABB") {
			geraldJap.text = "You win...\nLet's never speak\nof this again.";
			doorSays.text = "Sure,\nbut you owe me one.\nBy the way, shouldn't you\nbe somewhere by now?";
			ot1.text = "GO OUTSIDE";
			option2.transform.localScale = new Vector3 (0, 0, 0);
			option3.transform.localScale = new Vector3 (0, 0, 0);
			option4.transform.localScale = new Vector3 (0, 0, 0);
			key = "ABBA";
		} else if (key == "ABBA") {
			SceneManager.LoadScene ("OutsideFinal");
		} else if (key == "ABC") {
			geraldJap.text = "...";
			doorSays.text = "...go.";
			ot1.text = "GO OUTSIDE";
			option2.transform.localScale = new Vector3 (0, 0, 0);
			option3.transform.localScale = new Vector3 (0, 0, 0);
			option4.transform.localScale = new Vector3 (0, 0, 0);
			key = "ABCA";
		} else if (key == "ABCA") {
			SceneManager.LoadScene ("OutsideFinal");
		} else if (key == "ABD") {
			geraldJap.text = "I will";
			doorSays.text = "Oh god\nplease no!\nI have children to feed!\nI'm sorry!\nYou can go!";
			ot1.text = "GO OUTSIDE";
			option2.transform.localScale = new Vector3 (0, 0, 0);
			option3.transform.localScale = new Vector3 (0, 0, 0);
			option4.transform.localScale = new Vector3 (0, 0, 0);
			key = "ABDA";
		} else if (key == "ABDA") {
			SceneManager.LoadScene ("OutsideFinal");
		} else if (key == "AC") {
			geraldJap.text = "No.\nYou are a door.\nI command you to open!";
			doorSays.text = "Yes,\nmy lord!";
			ot1.text = "GO OUTSIDE";
			option2.transform.localScale = new Vector3 (0, 0, 0);
			option3.transform.localScale = new Vector3 (0, 0, 0);
			option4.transform.localScale = new Vector3 (0, 0, 0);
			key = "ACA";
		} else if (key == "ACA") {
			SceneManager.LoadScene ("OutsideFinal");
		}
		// //

		// B section //
		else if (key == "B") {
			geraldJap.text = "What's\nhappening!";
			doorSays.text = "You are\reflecting your fears\nand your subconscious\nis creating me. ";
			ot1.text = "Cool?";
			ot2.text = "I'm a mess...";
			option3.transform.localScale = new Vector3 (0, 0, 0);
			option4.transform.localScale = new Vector3 (0, 0, 0);
			key = "BA";
		} else if (key == "BA") {
			geraldJap.text = "Cool?";
			doorSays.text = "Not really.\nWhen you go,\nI'll die.";
			ot1.text = "But I need to go";
			ot2.text = "Then I'll stay";
			option3.transform.localScale = new Vector3 (0, 0, 0);
			option4.transform.localScale = new Vector3 (0, 0, 0);
			key = "BAA";
		} else if (key == "BAA") {
			geraldJap.text = "But I\nneed to go.\nMy school starts today.\nI hope you understand.";
			doorSays.text = "Yeah...\n   \n...sure.\n...good bye.";
			ot1.text = "GO OUTSIDE";
			option2.transform.localScale = new Vector3 (0, 0, 0);
			option3.transform.localScale = new Vector3 (0, 0, 0);
			option4.transform.localScale = new Vector3 (0, 0, 0);
			key = "BAAA";
		} else if (key == "BAAA") {
			SceneManager.LoadScene ("OutsideFinal");
		} else if (key == "BAAB") {
			geraldJap.text = "OK.";
			doorSays.text = "...bye.";
			ot1.text = "GO OUTSIDE";
			option2.transform.localScale = new Vector3 (0, 0, 0);
			option3.transform.localScale = new Vector3 (0, 0, 0);
			option4.transform.localScale = new Vector3 (0, 0, 0);
			key = "BAABA";
		} else if (key == "BAABA") {
			SceneManager.LoadScene ("OutsideFinal");
		}
		// //

		// C section //
		else if (key == "C") {
			geraldJap.text = "What?\nYou want me\nto open up to a door?";
			doorSays.text = "On second thought,\ndon't.\nJust leave.";
			ot1.text = "GO OUTSIDE";
			option2.transform.localScale = new Vector3 (0, 0, 0);
			option3.transform.localScale = new Vector3 (0, 0, 0);
			option4.transform.localScale = new Vector3 (0, 0, 0);
			key = "CA";
		} else if (key == "CA") {
			SceneManager.LoadScene ("OutsideFinal");
		} else if (key == "CB") {
			geraldJap.text = "You.\nA creepy talking door.";
			doorSays.text = "I'm part\nof your brain,\nso, you are the creepy one.\nGood bye.";
			ot1.text = "GO OUTSIDE";
			option2.transform.localScale = new Vector3 (0, 0, 0);
			option3.transform.localScale = new Vector3 (0, 0, 0);
			option4.transform.localScale = new Vector3 (0, 0, 0);
			key = "CBA";
		} else if (key == "CBA") {
			SceneManager.LoadScene ("OutsideFinal");
		} else if (key == "CC") {
			geraldJap.text = "You little\ncreepy creep you.";
			doorSays.text = "Leave\nme alone...";
			ot1.text = "GO OUTSIDE";
			option2.transform.localScale = new Vector3 (0, 0, 0);
			option3.transform.localScale = new Vector3 (0, 0, 0);
			option4.transform.localScale = new Vector3 (0, 0, 0);
			key = "CCA";
		} else if (key == "CCA") {
			SceneManager.LoadScene ("OutsideFinal");
		}
		// //

		// D section //
		else if (key == "D") {
			geraldJap.text = "Nope,\nnever";
			doorSays.text = "Are you sure?\nI'm really big in Spain.";
			ot1.text = "Ahhh! Right!";
			ot2.text = "Still a ''no''";
			option3.transform.localScale = new Vector3 (0, 0, 0);
			option4.transform.localScale = new Vector3 (0, 0, 0);
			key = "DA";
		} else if (key == "DA") {
			geraldJap.text = "Aaah!\nRight!\nSenior Puerta!";
			doorSays.text = "You have\nseen my show?\nFantastic!\nWe'll talk more later.\nNow you must\nmake haste.";
			ot1.text = "GO OUTSIDE";
			option2.transform.localScale = new Vector3 (0, 0, 0);
			option3.transform.localScale = new Vector3 (0, 0, 0);
			option4.transform.localScale = new Vector3 (0, 0, 0);
			key = "DAA";
		} else if (key == "DAA") {
			SceneManager.LoadScene ("OutsideFinal");
		} else if (key == "DB") {
			geraldJap.text = "I'm\nreally sorry.\nI didn't mean to bring back\nbad memories.";
			doorSays.text = "( \"Click\" )\n( \"Bang\" )\n( \"Thump\" )";
			ot1.text = "GO OUTSIDE";
			option2.transform.localScale = new Vector3 (0, 0, 0);
			option3.transform.localScale = new Vector3 (0, 0, 0);
			option4.transform.localScale = new Vector3 (0, 0, 0);
			key = "DBA";
		} else if (key == "DBA") {
			SceneManager.LoadScene ("OutsideFinal");
		}
		// //
	}
	/// <summary>
	/// works with right key
	/// </summary>
	/// <param name="two">Two.</param>
	private void option2Clicked(Button two) {  //Button two, AKA button B //
		// A section //
		if (key == "A") {
			geraldJap.text = "...";
			doorSays.text = "SPEAK\nYOU FOOL!!!";
			ot1.text = "I am not!";
			ot2.text = "Fool?";
			ot3.text = "...";
			ot4.text = "That's mean";
			key = "AB";
		} else if (key == "ABA") {
			geraldJap.text = "\"Sniff,\nsniff\".";
			doorSays.text = "Oh,\nI'm terribly sorry.\nIt was all a joke.\nSorry...";
			option1.transform.localScale = new Vector3 (0, 0, 0);
			ot2.text = "GO OUTSIDE";
			option3.transform.localScale = new Vector3 (0, 0, 0);
			option4.transform.localScale = new Vector3 (0, 0, 0);
			key = "ABAB";
		} else if (key == "ABAB") {
			SceneManager.LoadScene ("OutsideFinal");
		} else if (key == "AA") {
			geraldJap.text = "Can't I just go? Please!?\nWhat have I done to deserve\nyou?";
			doorSays.text = "I'm\njust a door.\nYou could have opened\nme at any time...\n...You prick.";
			option1.transform.localScale = new Vector3 (0, 0, 0);
			ot2.text = "GO OUTSIDE";
			option3.transform.localScale = new Vector3 (0, 0, 0);
			option4.transform.localScale = new Vector3 (0, 0, 0);
			key = "AAB";
		} else if (key == "AAB") {
			SceneManager.LoadScene ("OutsideFinal");
		} else if (key == "AAC") {
			geraldJap.text = "Well Boo Hoo!!!\nI need to go!\nNOW!!!";
			doorSays.text = "Whoa!!\nYou're a prick!!\nI thought we had\nsomething here.\nBeat it son!";
			option1.transform.localScale = new Vector3 (0, 0, 0);
			ot2.text = "GO OUTSIDE";
			option3.transform.localScale = new Vector3 (0, 0, 0);
			option4.transform.localScale = new Vector3 (0, 0, 0);
			key = "AACB";
		} else if (key == "AACB") {
			SceneManager.LoadScene ("OutsideFinal");
		} else if (key == "AB") {
			geraldJap.text = "Fool?\nI'm the fool one?\nWell... You...\nSuck... er...";
			doorSays.text = "Nice one...";
			ot1.text = "You win...";
			ot2.text = "Just let me pass";
			option3.transform.localScale = new Vector3 (0, 0, 0);
			option4.transform.localScale = new Vector3 (0, 0, 0);
			key = "ABB";
		} else if (key == "ABB") {
			geraldJap.text = "Yeah, yeah...\nJust let me pass.";
			doorSays.text = "The fool\ncan leave.";
			option1.transform.localScale = new Vector3 (0, 0, 0);
			ot2.text = "GO OUTSIDE";
			option3.transform.localScale = new Vector3 (0, 0, 0);
			option4.transform.localScale = new Vector3 (0, 0, 0);
			key = "ABBB";
		} else if (key == "ABBB") {
			SceneManager.LoadScene ("OutsideFinal");
		} else if (key == "ABC") {
			geraldJap.text = ".......\n.............\n.....BOOO!!!";
			doorSays.text = "AAiiiiiirgggh!!!\n(heart attack)";
			option1.transform.localScale = new Vector3 (0, 0, 0);
			ot2.text = "GO OUTSIDE";
			option3.transform.localScale = new Vector3 (0, 0, 0);
			option4.transform.localScale = new Vector3 (0, 0, 0);
			key = "ABCB";
		} else if (key == "ABCB") {
			SceneManager.LoadScene ("OutsideFinal");
		} else if (key == "ABD") {
			geraldJap.text = "...damn.\nI hate you.";
			doorSays.text = "No you don't.\nYou hate yourself.\nNow you can go.";
			option1.transform.localScale = new Vector3 (0, 0, 0);
			ot2.text = "GO OUTSIDE";
			option3.transform.localScale = new Vector3 (0, 0, 0);
			option4.transform.localScale = new Vector3 (0, 0, 0);
			key = "ABDB";
		} else if (key == "ABDB") {
			SceneManager.LoadScene ("OutsideFinal");
		} else if (key == "AC") {
			geraldJap.text = "Can I go,\nplease?";
			doorSays.text = "That a boy.";
			option1.transform.localScale = new Vector3 (0, 0, 0);
			ot2.text = "GO OUTSIDE";
			option3.transform.localScale = new Vector3 (0, 0, 0);
			option4.transform.localScale = new Vector3 (0, 0, 0);
			key = "ACB";
		} else if (key == "ACB") {
			SceneManager.LoadScene ("OutsideFinal");
		}
		// // 

		// B section //
		else if (key == "X") {  
			geraldJap.text = "AAAaaargh!!!";
			doorSays.text = "Whoa!";
			ot1.text = "What's happening?";
			ot2.text = "Am I dead?";
			ot3.text = "Don't eat me!";
			option4.transform.localScale = new Vector3 (0, 0, 0);
			key = "B";
		} else if (key == "B") {  
			geraldJap.text = "Am I dead?";
			doorSays.text = "Naah,\nbut you wish.\nHave a nice day at school.";
			option1.transform.localScale = new Vector3 (0, 0, 0);
			ot2.text = "GO OUTSIDE";
			option3.transform.localScale = new Vector3 (0, 0, 0);
			option4.transform.localScale = new Vector3 (0, 0, 0);
			key = "BB";
		} else if (key == "BB") {  
			SceneManager.LoadScene ("OutsideFinal");
		} else if (key == "BA") {
			geraldJap.text = "I'm a mess.\nAm I not?";
			doorSays.text = "Yeah,\nyou kinda are...\n...but go,\nand try not to be.";
			option1.transform.localScale = new Vector3 (0, 0, 0);
			ot2.text = "GO OUTSIDE";
			option3.transform.localScale = new Vector3 (0, 0, 0);
			option4.transform.localScale = new Vector3 (0, 0, 0);
			key = "BAB";
		} else if (key == "BAB") {
			SceneManager.LoadScene ("OutsideFinal");
		} else if (key == "BAA") {
			geraldJap.text = "Then\nI'll stay.\nI'll save you";
			doorSays.text = "No, you must go.\nMake your mother\nproud.";
			ot1.text = "OK";
			ot2.text = "I'll miss you";
			option3.transform.localScale = new Vector3 (0, 0, 0);
			option4.transform.localScale = new Vector3 (0, 0, 0);
			key = "BAAB";
		} else if (key == "BAAB") {
			geraldJap.text = "I'll\nmiss you,\nyou weird ominous\ndevil of a door.";
			doorSays.text = "Tell \nmy wife\nthat I love her.";
			option1.transform.localScale = new Vector3 (0, 0, 0);
			ot2.text = "GO OUTSIDE";
			option3.transform.localScale = new Vector3 (0, 0, 0);
			option4.transform.localScale = new Vector3 (0, 0, 0);
			key = "BAABB";
		} else if (key == "BAABB") {
			SceneManager.LoadScene ("OutsideFinal");
		}
		// //

		// C section //
		else if (key == "C") {
			geraldJap.text = "I'm\nscared.";
			doorSays.text = "What are\nyou scared about?";
			ot1.text = "You";
			ot2.text = "Me";
			ot3.text = "Everybody";
			option4.transform.localScale = new Vector3 (0, 0, 0);
			key = "CB";
		} else if (key == "CB") {
			geraldJap.text = "Me.\nI'm probably\nhallucinating\nright now.";
			doorSays.text = "Yep.\nYou can just\nopen me\nand go.";
			option1.transform.localScale = new Vector3 (0, 0, 0);
			ot2.text = "GO OUTSIDE";
			option3.transform.localScale = new Vector3 (0, 0, 0);
			option4.transform.localScale = new Vector3 (0, 0, 0);
			key = "CBB";
		} else if (key == "CBB") {
			SceneManager.LoadScene ("OutsideFinal");
		} else if (key == "CC") {
			geraldJap.text = "Really?";
			doorSays.text = "No.\nI'm a creep\nand the creep says\n\"bye\".";
			option1.transform.localScale = new Vector3 (0, 0, 0);
			ot2.text = "GO OUTSIDE";
			option3.transform.localScale = new Vector3 (0, 0, 0);
			option4.transform.localScale = new Vector3 (0, 0, 0);
			key = "CCB";
		} else if (key == "CCB") {
			SceneManager.LoadScene ("OutsideFinal");
		}
		// //

		// D section //
		else if (key == "D") {
			geraldJap.text = "Doorious?\nYour parents\nreally must have\nhated you.";
			doorSays.text = "I gave\nthat name to myself...\nI never had parents...\n...\"sniff\".";
			ot1.text = "I'm sorry";
			ot2.text = "Anyways...";
			option3.transform.localScale = new Vector3 (0, 0, 0);
			option4.transform.localScale = new Vector3 (0, 0, 0);
			key = "DB";
		} else if (key == "DB") {
			geraldJap.text = "Anyways...\nI have an appointment\nin a while.\nSo...";
			doorSays.text = "You\ndon't need to\nbeat around the bush.\nI get it.\nJust go.";
			option1.transform.localScale = new Vector3 (0, 0, 0);
			ot2.text = "GO OUTSIDE";
			option3.transform.localScale = new Vector3 (0, 0, 0);
			option4.transform.localScale = new Vector3 (0, 0, 0);
			key = "DBB";
		} else if (key == "DBB") {
			SceneManager.LoadScene ("OutsideFinal");
		} else if (key == "DA") {
			geraldJap.text = "Still a \"no\".";
			doorSays.text = "I must\neducate you young man\nafter you get back\nfrom school.";
			option1.transform.localScale = new Vector3 (0, 0, 0);
			ot2.text = "GO OUTSIDE";
			option3.transform.localScale = new Vector3 (0, 0, 0);
			option4.transform.localScale = new Vector3 (0, 0, 0);
			key = "DAB";
		} else if (key == "DAB") {
			SceneManager.LoadScene ("OutsideFinal");
		}
		// //
	}
	/// <summary>
	/// Works with right key
	/// </summary>
	/// <param name="three">Three.</param>
	private void option3Clicked(Button three){
		// A section //
		if (key == "AA") {
			geraldJap.text = "Come on...\nI really need to go.";
			doorSays.text = "But I don't want you to...";
			ot1.text = "What!? Why!?";
			ot2.text = "Well Boo Hoo!";
			option3.transform.localScale = new Vector3 (0, 0, 0);
			option4.transform.localScale = new Vector3 (0, 0, 0);
			key = "AAC";
		} else if (key == "A") {
			geraldJap.text = "OUTSIDE!!!\nI need to go\noutside!";
			doorSays.text = "Mind your\nmanners young man!";
			ot1.text = "No";
			ot2.text = "Please?";
			ot3.text = "Realy...";
			option4.transform.localScale = new Vector3 (0, 0, 0);
			key = "AC";
		} else if (key == "AB") {
			geraldJap.text = "...";
			doorSays.text = "AAAaaaaarrrggh!\nWhy won't you speak?\nSPEAK!!!";
			ot1.text = "...";
			ot2.text = "BOOOO!!!";
			option3.transform.localScale = new Vector3 (0, 0, 0);
			option4.transform.localScale = new Vector3 (0, 0, 0);
			key = "ABC";
		} else if (key == "AC") {
			geraldJap.text = "Really...\nOpen sesame?";
			doorSays.text = "Fooosh!";
			option1.transform.localScale = new Vector3 (0, 0, 0);
			option2.transform.localScale = new Vector3 (0, 0, 0);
			ot3.text = "GO OUTSIDE";
			option4.transform.localScale = new Vector3 (0, 0, 0);
			key = "ACC";
		} else if (key == "ACC") {
			SceneManager.LoadScene ("OutsideFinal");
		}
		// //

		// B section //
		else if (key == "B") {
			geraldJap.text = "Don't eat me!\nDon't eat me!\nDon't eat me!";
			doorSays.text = "Shut up and go!";
			option1.transform.localScale = new Vector3 (0, 0, 0);
			option2.transform.localScale = new Vector3 (0, 0, 0);
			ot3.text = "GO OUTSIDE";
			option4.transform.localScale = new Vector3 (0, 0, 0);
			key = "BC";
		} else if (key == "BC") {
			SceneManager.LoadScene ("OutsideFinal");
		}
		// //

		// C section //
		else if (key == "X") {  
			geraldJap.text = "Just forget it...";
			doorSays.text = "What?\nYou can tell me.\nI'm a great listener.";
			ot1.text = "What?";
			ot2.text = "I'm scared";
			ot3.text = "Creep";
			option4.transform.localScale = new Vector3 (0, 0, 0);
			key = "C";
		} else if (key == "C") {
			geraldJap.text = "Creep...\nYou little creepy creep.";
			doorSays.text = "I am not...";
			ot1.text = "Creepy creep";
			ot2.text = "Really?";
			option3.transform.localScale = new Vector3 (0, 0, 0);
			option4.transform.localScale = new Vector3 (0, 0, 0);
			key = "CC";
		} else if (key == "CB") {
			geraldJap.text = "I'm scared of everybody.";
			doorSays.text = "Oh, well...\nTough luck man.\nI'll resume back to\nbeing a regular door.\nBye.";
			option1.transform.localScale = new Vector3 (0, 0, 0);
			option2.transform.localScale = new Vector3 (0, 0, 0);
			ot3.text = "GO OUTSIDE";
			option4.transform.localScale = new Vector3 (0, 0, 0);
			key = "CBC";
		} else if (key == "CBC") {
			SceneManager.LoadScene ("OutsideFinal");
		}
		// //

		// D section //
		else if (key == "D") {
			geraldJap.text = "I'll chop you,\nif you won't\nlet me pass,\nMr. Doorious.";
			doorSays.text = "Aaah!\nNot in the face!\nI have family!\nHere's all my money!\nPlease go!";
			option1.transform.localScale = new Vector3 (0, 0, 0);
			option2.transform.localScale = new Vector3 (0, 0, 0);
			ot3.text = "GO OUTSIDE";
			option4.transform.localScale = new Vector3 (0, 0, 0);
			key = "DC";
		} else if (key == "DC") {
			SceneManager.LoadScene ("OutsideFinal");
		}
		// //
	}
	/// <summary>
	/// button works with right key
	/// </summary>
	/// <param name="four">Four.</param>
	private void option4Clicked(Button four){
		// A section //
		if (key == "A") {
			geraldJap.text = "You\nare a door,\nhence you have\nno ears.\nNot my fault.";
			doorSays.text = "I'm part\nof your own subconscious,\nyou piece of garbage.\nGET OUT!";
			option1.transform.localScale = new Vector3 (0, 0, 0);
			option2.transform.localScale = new Vector3 (0, 0, 0);
			option3.transform.localScale = new Vector3 (0, 0, 0);
			ot4.text = "GO OUTSIDE";
			key = "AD";
		} else if (key == "AD") {
			SceneManager.LoadScene ("OutsideFinal");
		} else if (key == "AA") {
			geraldJap.text = "I'd like\nto go there.\nIf you don't mind?";
			doorSays.text = "Why\ndidn't you say so\nin the first place.\nHere you go.";
			option1.transform.localScale = new Vector3 (0, 0, 0);
			option2.transform.localScale = new Vector3 (0, 0, 0);
			option3.transform.localScale = new Vector3 (0, 0, 0);
			ot4.text = "GO OUTSIDE";
			key = "AAD";
		} else if (key == "AAD") {
			SceneManager.LoadScene ("OutsideFinal");
		} else if (key == "AB") {
			geraldJap.text = "That's mean.\nYou're mean.";
			doorSays.text = "Sue me";
			ot1.text = "I will";
			ot2.text = "...damn.";
			option3.transform.localScale = new Vector3 (0, 0, 0);
			option4.transform.localScale = new Vector3 (0, 0, 0);
			key = "ABD";
		}
		// //

		// B section //

		// //

		// C section //

		// //

		// D section //
		else if (key == "X") {
			geraldJap.text = "What?\nWho are you?";
			doorSays.text = "I am\nMr. Doorius.\nDoor for short.\nMaybe you have heard \nof me?";
			ot1.text = "Nope";
			ot2.text = "Doorious?";
			ot3.text = "I'll chop you";
			option4.transform.localScale = new Vector3 (0, 0, 0);
			key = "D";
		} 
		// //
	}
	/// <summary>
	/// hides inventory, shows canvases
	/// </summary>
	/// <param name="batman">Batman.</param>
	private void OnTriggerEnter (Collider batman) {
		if (batman.tag == "gerald"){
			inventoryCan.enabled = false;
			doorVas.enabled = true;
			geraldVas.enabled = true;
		}
	}
	/// <summary>
	/// shows inventory, hides canvases
	/// </summary>
	/// <param name="batman">Batman.</param>
	private void OnTriggerExit (Collider batman) {
		if (batman.tag == "gerald") {
			inventoryCan.enabled = true;
			doorVas.enabled = false;
			geraldVas.enabled = false;
		}
	}

}