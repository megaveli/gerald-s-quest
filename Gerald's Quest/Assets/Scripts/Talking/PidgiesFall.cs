﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PidgiesFall : MonoBehaviour {

	private Rigidbody pidgi;
	private GameObject bird;



	void Start () {
		
		pidgi = (Rigidbody)GameObject.Find ("pidgi").GetComponent<Rigidbody> ();
		bird = GameObject.Find ("pidgi");

	}
	/// <summary>
	/// entering collider activates gravity in rigidbody
	/// </summary>
	/// <param name="batman">Batman.</param>
	private void OnTriggerEnter (Collider batman) {
		if (batman.tag == "gerald") {
			
			pidgi.useGravity = true;
		}
	}
	/// <summary>
	/// exiting collider destroyss GameObjest
	/// </summary>
	/// <param name="batman">Batman.</param>
	private void OnTriggerExit (Collider batman) {
		if (batman.tag == "gerald") {
			Destroy (bird);

		}
	}


}
