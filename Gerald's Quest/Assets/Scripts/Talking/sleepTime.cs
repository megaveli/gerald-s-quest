﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class sleepTime : MonoBehaviour {

	private Canvas geraldVas;

	void Start () {

		geraldVas = (Canvas)GameObject.Find ("geraldVas").GetComponent<Canvas> ();

		geraldVas.enabled = false;

	}
	/// <summary>
	/// shows canvas
	/// </summary>
	/// <param name="batman">Batman.</param>
	private void OnTriggerEnter (Collider batman) {
		if (batman.tag == "gerald") {
			geraldVas.enabled = true;
		}
	}
	/// <summary>
	/// hides canvas
	/// </summary>
	/// <param name="batman">Batman.</param>
	private void OnTriggerExit (Collider batman) {
		if (batman.tag == "gerald") {
			geraldVas.enabled = false;
		}
	}
}
