﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Janice : MonoBehaviour {

	private GameObject playerScore;
	private Player player;
	private Canvas inventoryCan;
	private Canvas geraldVas;
	private Canvas janiceVas;
	private RawImage geraldBubble;
	private RawImage janiceBubble;
	private Button optionA;
	private Button optionB;
	private Button optionC;
	private Text geraldJap;
	private Text janiceRap;
	private Text ot1;
	private Text ot2;
	private Text ot3;
	private BoxCollider janice;
	private BoxCollider door;
	private string key;
	private int end;
	private GameObject number;

	void Start () {

		playerScore = GameObject.Find ("PlayerScore");
		player = playerScore.GetComponent<Player> ();

		inventoryCan = (Canvas)GameObject.Find ("Canvas").GetComponent<Canvas> ();
		geraldVas = (Canvas)GameObject.Find ("geraldVas").GetComponent<Canvas> ();
		janiceVas = (Canvas)GameObject.Find ("janiceVas").GetComponent<Canvas> ();

		geraldBubble = (RawImage)GameObject.Find ("geraldBubble").GetComponent<RawImage> ();
		janiceBubble = (RawImage)GameObject.Find ("janiceBubble").GetComponent<RawImage> ();

		optionA = (Button)GameObject.Find ("optionA").GetComponent<Button> ();
		optionB = (Button)GameObject.Find ("optionB").GetComponent<Button> ();
		optionC = (Button)GameObject.Find ("optionC").GetComponent<Button> ();

		optionA.onClick.AddListener (() => optionAClicked (optionA));
		optionB.onClick.AddListener (() => optionBClicked (optionB));
		optionC.onClick.AddListener (() => optionCClicked (optionC));

		geraldJap = (Text)GameObject.Find ("geraldJap").GetComponent<Text> ();
		janiceRap = (Text)GameObject.Find ("janiceRap").GetComponent<Text> ();

		ot1 = (Text)GameObject.Find ("ot1").GetComponent<Text> ();
		ot2 = (Text)GameObject.Find ("ot2").GetComponent<Text> (); 
		ot3 = (Text)GameObject.Find ("ot3").GetComponent<Text> (); 

		door = (BoxCollider)GameObject.Find ("door").GetComponent<BoxCollider> ();
		janice = (BoxCollider)GameObject.Find ("janice").GetComponent<BoxCollider> ();

		number = GameObject.Find ("number");

		geraldVas.enabled = false;
		janiceVas.enabled = false;
		number.transform.localScale = new Vector3 (0, 0, 0);
		door.transform.localScale = new Vector3 (0, 0, 0);

		key = "X";
		end = 0;

		/*
		geraldJap.text = "";
		janiceRap.text = "";
		ot1.text = "";
		ot2.text = "";
		ot3.text = "";
		key = "";
		*/
	}
	/// <summary>
	/// Hides buttons, enables collider, shows room number,
	/// </summary>
	public void OpenDoor(){
		optionA.transform.localScale = new Vector3 (0, 0, 0);
		optionB.transform.localScale = new Vector3 (0, 0, 0);
		optionC.transform.localScale = new Vector3 (0, 0, 0);
		number.transform.localScale = new Vector3 (1, 1, 1);
		end++;
		door.transform.localScale = new Vector3 (1, 1, 1);
	}
	/// <summary>
	/// Runs with right keys
	/// </summary>
	/// <param name="A">A.</param>
	private void optionAClicked(Button A){
		if (key == "X") {  // 1 //
			geraldJap.text = "Where's my\nclassroom?";
			janiceRap.text = "What?\nHow should I know?";
			ot1.text = "But you work here...";
			ot2.text = "What do I do now?";
			optionC.transform.localScale = new Vector3 (0, 0, 0);
			key = "A";
		} else if (key == "A") {  // 2 //
			geraldJap.text = "But you\nwork here.\nClass \"404\", please?";
			janiceRap.text = "Shut it\nshrimp!\nIt's behind me!\nLeave me\nalone!";
			OpenDoor ();
			player.SetConf (5);
			//END//
		} else if (key == "B") {  // 3 //
			geraldJap.text = "I mean...\n...help.";
			janiceRap.text = "Help?\nYou?";
			ot1.text = "Meeeeee?";
			ot2.text = "Please?";
			optionC.transform.localScale = new Vector3 (0, 0, 0);
			key = "BA";
		} else if (key == "C") {  // 4 //
			geraldJap.text = "Yes.";
			janiceRap.text = "Well,\nyou're right.\nIt's just behind me.\nHave a nice\nday.";
			OpenDoor ();
			player.ChangeConf (20);
			//END//
		} else if (key == "BA") {  // 5 //
			geraldJap.text = "Meeeeee?";
			janiceRap.text = "...nevermind.";
			OpenDoor ();
			player.ChangeConf (-5);
			//END//
		} else if (key == "BB") {  // 6 //
			geraldJap.text = "Damn!";
			janiceRap.text = "Hihihihih!\nIt's over there.";
			OpenDoor ();
			//END//
		} else if (key == "CB") {  // 7 //
			geraldJap.text = "Yes.";
			janiceRap.text = "You are...\n...absolutely right!\nMuhahahahah!";
			OpenDoor ();
			player.ChangeConf (5);
			//END//
		}
	}
	/// <summary>
	/// Runs with right keys
	/// </summary>
	/// <param name="B">B.</param>
	private void optionBClicked(Button B){
		if (key == "X") {  // 1 //
			geraldJap.text = "Umm....\nNnngh...";
			janiceRap.text = "O---ka-y?";
			ot1.text = "I mean...\n...help?";
			ot2.text = "Don't judge me";
			optionC.transform.localScale = new Vector3 (0, 0, 0);
			key = "B";
		} else if (key == "A") {  // 2 //
			geraldJap.text = "What do I\ndo now?";
			janiceRap.text = "Look\naround,\nyou dumdum.";
			OpenDoor ();
			player.ChangeConf (-15);
			//END//
		} else if (key == "B") {  // 3 //
			geraldJap.text = "Don't\njudge me!";
			janiceRap.text = "Already did.";
			ot1.text = "Damn!";
			ot2.text = "...class \"404\"?";
			optionC.transform.localScale = new Vector3 (0, 0, 0);
			key = "BB";
		} else if (key == "C") {  // 4 // 
			geraldJap.text = "Of course not.";
			janiceRap.text = "Are you\nsaying, that I'm\na bad person?";
			ot1.text = "Yes";
			ot2.text = "No";
			optionC.transform.localScale = new Vector3 (0, 0, 0);
			key = "CB";
		} else if (key == "BA") {  // 5 //
			geraldJap.text = "Please?\nI'm lost.";
			janiceRap.text = "Don't\nask me.\nI just work here.";
			OpenDoor ();
			//END//
		} else if (key == "BB") {  // 6 //
			geraldJap.text = ".......\n....class \"404\"?";
			janiceRap.text = "HA!\nNot telling you\nNya! Nya!";
			OpenDoor ();
			player.ChangeConf (-5);
			//END//
		} else if (key == "CB") {  // 7 //
			geraldJap.text = "No.";
			janiceRap.text = "Stop\ndefining me!\nIt's behind me, you monster!";
			OpenDoor ();
			player.ChangeConf (-10);
			//END//
		}
	}
	/// <summary>
	/// Runs with right key.
	/// </summary>
	/// <param name="C">C.</param>
	private void optionCClicked(Button C){
		if (key == "X") {  // 1 //
			geraldJap.text = "Can you\nhelp me?\nI have\ntrouble finding\nthe classroom \"404\"";
			janiceRap.text = "Do\nI look like\na charity worker?";
			ot1.text = "Yes";
			ot2.text = "Of course not";
			optionC.transform.localScale = new Vector3 (0, 0, 0);
			key = "C";
		} 
	}
	/// <summary>
	/// Entering collider hides inventory, enables canvases.
	/// </summary>
	/// <param name="batman">Batman.</param>
	private void OnTriggerEnter (Collider batman) {
		if (batman.tag == "gerald") {
			inventoryCan.enabled = false;
			geraldVas.enabled = true;
			janiceVas.enabled = true;
		}
	}
	/// <summary>
	/// On exiting collider shows inventory, hides canvases, if right value: destroys collider
	/// </summary>
	/// <param name="batman">Batman.</param>
	private void OnTriggerExit (Collider batman) {
		if (batman.tag == "gerald") {
			inventoryCan.enabled = true;
			geraldVas.enabled = false;
			janiceVas.enabled = false;
			if (end == 1) {
				Destroy (janice);
			}
		}
	}
}
