﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class infoTalk : MonoBehaviour {

	private Canvas inventoryCan;
	private Canvas infoVas;
	private RawImage infoBubble;

	void Start () {

		inventoryCan = (Canvas)GameObject.Find ("Canvas").GetComponent<Canvas> ();
		infoVas = (Canvas)GameObject.Find ("infoVas").GetComponent<Canvas> ();
		infoVas.enabled = false;

	}
		/// <summary>
		/// Entering collider activates canvas and hides inventory.
		/// </summary>
		/// <param name="batman">Batman.</param>
	private void OnTriggerEnter (Collider batman) {
		if (batman.tag == "gerald") {
			inventoryCan.enabled = false;
			infoVas.enabled = true;
		}
	}
	/// <summary>
	/// Exiting collider hides canvas and shows inventory.
	/// </summary>
	/// <param name="batman">Batman.</param>
	private void OnTriggerExit (Collider batman) {
		if (batman.tag == "gerald") {
			inventoryCan.enabled = true;
			infoVas.enabled = false;
		}
	}
}
