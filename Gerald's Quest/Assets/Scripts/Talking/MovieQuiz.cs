﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MovieQuiz : MonoBehaviour {

	private GameObject playerScore;
	private Player player;
	private Canvas inventoryCan;
	private Canvas geraldVas;
	private Canvas mindyVas;
	private RawImage geraldBubble;
	private RawImage mindyBubble;
	private Button optionA;
	private Button optionB;
	private Button optionC;
	private Text geraldJap;
	private Text mindyRap;
	private Text aText;
	private Text bText;
	private Text cText;
	private BoxCollider mindyCol;
	private BoxCollider afterQuiz;
	private string key;
	private int round;
	private int points;

	void Start () {

		playerScore = GameObject.Find ("PlayerScore");
		player = playerScore.GetComponent<Player> ();

		inventoryCan = (Canvas)GameObject.Find ("Canvas").GetComponent<Canvas> ();
		geraldVas = (Canvas)GameObject.Find ("geraldVas").GetComponent<Canvas> ();
		mindyVas = (Canvas)GameObject.Find ("mindyVas").GetComponent<Canvas> ();

		geraldBubble = (RawImage)GameObject.Find ("geraldBubble").GetComponent<RawImage> ();
		mindyBubble = (RawImage)GameObject.Find ("mindyBubble").GetComponent<RawImage> ();

		optionA = (Button)GameObject.Find ("optionA").GetComponent<Button> ();
		optionB = (Button)GameObject.Find ("optionB").GetComponent<Button> ();
		optionC = (Button)GameObject.Find ("optionC").GetComponent<Button> ();

		optionA.onClick.AddListener (() => optionAClicked (optionA));
		optionB.onClick.AddListener (() => optionBClicked (optionB));
		optionC.onClick.AddListener (() => optionCClicked (optionC));

		geraldJap = (Text)GameObject.Find ("geraldJap").GetComponent<Text> ();
		mindyRap = (Text)GameObject.Find ("mindyRap").GetComponent<Text> ();

		aText = (Text)GameObject.Find ("aText").GetComponent<Text> ();
		bText = (Text)GameObject.Find ("bText").GetComponent<Text> (); 
		cText = (Text)GameObject.Find ("cText").GetComponent<Text> (); 

		afterQuiz = (BoxCollider)GameObject.Find ("afterQuiz").GetComponent<BoxCollider> ();
		mindyCol = (BoxCollider)GameObject.Find ("mindyCol").GetComponent<BoxCollider> ();

		geraldVas.enabled = false;
		mindyVas.enabled = false;

		afterQuiz.transform.localScale = new Vector3 (0, 0, 0);
		geraldBubble.transform.localScale = new Vector3 (0, 0, 0);

		key = "X";
		round = 0;
		points = 1;

	}
	/// <summary>
	/// Runs questions and checks points
	/// </summary>
	public void Quiz(){
		round++;
		if (round == 1) {
			mindyRap.text = "Who is\nthe most anoying\ncharacter in\nthe \"Star Wars\"-movies?";
			aText.text = "Jar Jar";
			bText.text = "3-CPO";
			cText.text = "Anakin";
		} else if (round == 2) {
			mindyRap.text = "In\nhow many movies,\nMorgan Freeman is a God?";
			aText.text = "1";
			bText.text = "2";
			cText.text = "3";
		} else if (round == 3) {
			mindyRap.text = "How many\n\"Indiana Jones\"-movies\nis there?";
			aText.text = "4";
			bText.text = "3";
			cText.text = "2";
		} else if (round == 4) {
			mindyRap.text = "What's\nin the box?";
			aText.text = "Wrath";
			bText.text = "Just a box";
			cText.text = "Chocolate";
		} else if (round == 5) {
			mindyRap.text = "What is\nthe name of\nthe wizard in LOTR?";
			aText.text = "Dumbledore";
			bText.text = "Gandalf";
			cText.text = "Saruman";
		} else if (round == 6) {
			mindyRap.text = "Fill the blank in \"Live long and -blank\".";
			aText.text = "Prosper";
			bText.text = "Reproduce";
			cText.text = "Suffer";
		} else if (round == 7) {
			mindyRap.text = "Which of these is a \"James Bond\"-movie?";
			aText.text = "\"Never say never\"";
			bText.text = "\"Never say never again\"";
			cText.text = "\"Always say never, again\n";
		} else if (round == 8) {
			mindyRap.text = "What is the best \"Saw\"-movie?";
			aText.text = "The first";
			bText.text = "The last";
			cText.text = "I never \"saw\" them...";
		} else if (round == 9) {
			mindyRap.text = "What makes you feel good?";
			aText.text = "Dustin'";
			bText.text = "Bustin'";
			cText.text = "Rustin'";
		} else if (round == 10) {
			mindyRap.text = "What is the subtitle of the movie \"Tenacious D\"?";
			aText.text = "Stick of destiny";
			bText.text = "Pick of destiny";
			cText.text = "Oh, well, you know...";
		} else if (round == 11) {
			mindyRap.text = "Want to play this again! Please!?";
			aText.text = "Naah";
			bText.text = "Maybe later...";
			cText.text = "I think I left my oven on.";
		} else if (round == 12) {
			mindyRap.text = "Well you got " + points + " out of 10. Bye!";
			optionA.transform.localScale = new Vector3 (0, 0, 0);
			optionB.transform.localScale = new Vector3 (0, 0, 0);
			optionC.transform.localScale = new Vector3 (0, 0, 0);
			afterQuiz.transform.localScale = new Vector3 (1, 1, 1);
			geraldBubble.transform.localScale = new Vector3 (1, 1, 1);
			geraldJap.text = "Bye.\n      bye.";
			player.ChangeConf (points * 2);
		}
		Debug.Log ("points " + points);
		Debug.Log ("round " + round);
	}
	/// <summary>
	/// Runs with right keys
	/// </summary>
	/// <param name="A">A.</param>
	private void optionAClicked(Button A){
		if (key == "X") {
			geraldBubble.transform.localScale = new Vector3 (1, 1, 1);
			geraldJap.text = "I'm Gerald.";
			mindyRap.text = "Let's play\na movie\nquiz!";
			aText.text = "O--ka-y.\nSure...";
			bText.text = "Do I\nhave to?";
			key = "Y";
		} else if (key == "Y") {
			geraldBubble.transform.localScale = new Vector3 (0, 0, 0);
			mindyRap.text = "It\nwasn't a question.";
			optionA.transform.localScale = new Vector3 (0, 0, 0);
			bText.text = "...";
			key = "Z";
		} else if (key == "Q") {
			Quiz ();
			if (round == 5) {
				points++;
			} else if (round == 6) {
				points++;
			} else if (round == 7) {
				points++;
			} else if (round == 9) {
				points++;
			}
		}
	}
	/// <summary>
	/// Runs with right keys
	/// </summary>
	/// <param name="B">B.</param>
	private void optionBClicked(Button B){
		if (key == "X") {
			geraldBubble.transform.localScale = new Vector3 (1, 1, 1);
			geraldJap.text = "Cool.";
			mindyRap.text = "Let's play\na movie\nquiz!";
			aText.text = "O--ka-y.\nSure...";
			bText.text = "Do I\nhave to?";
			key = "Y";
		} else if (key == "Y") {
			geraldBubble.transform.localScale = new Vector3 (0, 0, 0);
			mindyRap.text = "It\nwasn't a question.";
			optionA.transform.localScale = new Vector3 (0, 0, 0);
			bText.text = "...";
			key = "Z";
		} else if (key == "Z") {
			optionA.transform.localScale = new Vector3 (0.3f, 0.3f, 1);
			optionC.transform.localScale = new Vector3 (0.3f, 0.3f, 1);
			Quiz ();
			key = "Q";
		} else if (key == "Q") {
			Quiz ();
			if (round == 3) {
				points++;
			} else if (round == 4) {
				points++;
			} else if (round == 6) {
				points++;
			} else if (round == 8) {
				points++;
			} else if (round == 9) {
				points++;
			} else if (round == 10) {
				points++;
			} else if (round == 11) {
				points++;
			} 
		}
	}
	/// <summary>
	/// Runs with right key
	/// </summary>
	/// <param name="C">C.</param>
	private void optionCClicked(Button C){
		if (key == "Q") {
			Quiz ();
			if (round == 6) {
				points++;
			} else if (round == 9) {
				points++;
			}  
		}  
	}
	/// <summary>
	/// REnttering collider hides inventory, if right key: starts conversation.
	/// </summary>
	/// <param name="batman">Batman.</param>
	private void OnTriggerEnter (Collider batman) {
		if (batman.tag == "gerald") {
			inventoryCan.enabled = false;
			if (key == "X") {
				geraldVas.enabled = true;
				mindyVas.enabled = true;
				mindyRap.text = "Oh, hi!\nI'm Mindy.\nI like movies.";
				aText.text = "I'm Gerald";
				bText.text = "Cool";
				optionC.transform.localScale = new Vector3 (0, 0, 0);
			}
		}
	}
	/// <summary>
	/// Exiting collider shows inventory, hides canvases, if quiz completed: hide collider
	/// </summary>
	/// <param name="batman">Batman.</param>
	private void OnTriggerExit (Collider batman) {
		if (batman.tag == "gerald") {
			inventoryCan.enabled = true;
			geraldVas.enabled = false;
			mindyVas.enabled = false;
			if (round == 12) {
				mindyCol.transform.localScale = new Vector3 (0, 0, 0);
			}
		}
	}
}
