﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RobFood : MonoBehaviour {

	private GameObject playerScore;
	private Player player;
	private Canvas inventoryCan;
	private Canvas geraldVas;
	private Canvas robVas;
	private RawImage geraldBubble;
	private RawImage robBubble;
	private Button optionA;
	private Button optionB;
	private Text geraldJap;
	private Text robRap;
	private Text aText;
	private Text bText;
	private BoxCollider robCol;
	private BoxCollider done;
	private string key;

	void Start () {

		playerScore = GameObject.Find ("PlayerScore");
		player = playerScore.GetComponent<Player> ();

		inventoryCan = (Canvas)GameObject.Find ("Canvas").GetComponent<Canvas> ();
		geraldVas = (Canvas)GameObject.Find ("geraldVas").GetComponent<Canvas> ();
		robVas = (Canvas)GameObject.Find ("robVas").GetComponent<Canvas> ();

		geraldBubble = (RawImage)GameObject.Find ("geraldBubble").GetComponent<RawImage> ();
		robBubble = (RawImage)GameObject.Find ("robBubble").GetComponent<RawImage> ();

		optionA = (Button)GameObject.Find ("optionA").GetComponent<Button> ();
		optionB = (Button)GameObject.Find ("optionB").GetComponent<Button> ();

		optionA.onClick.AddListener (() => optionAClicked (optionA));
		optionB.onClick.AddListener (() => optionBClicked (optionB));

		geraldJap = (Text)GameObject.Find ("geraldJap").GetComponent<Text> ();
		robRap = (Text)GameObject.Find ("robRap").GetComponent<Text> ();

		aText = (Text)GameObject.Find ("aText").GetComponent<Text> ();
		bText = (Text)GameObject.Find ("bText").GetComponent<Text> ();  

		robCol = (BoxCollider)GameObject.Find ("robCol").GetComponent<BoxCollider> ();
		done = (BoxCollider)GameObject.Find ("done").GetComponent<BoxCollider> ();

		geraldVas.enabled = false;
		robVas.enabled = false;

		done.transform.localScale = new Vector3 (0, 0, 0);
		geraldBubble.transform.localScale = new Vector3 (0, 0, 0);

		key = "X";

	}
	/// <summary>
	/// runs conversation with right key	
	/// </summary>
	/// <param name="A">A.</param>
	private void optionAClicked (Button A){
		if (key == "X") {
			geraldBubble.transform.localScale = new Vector3 (0.6f, 0.6f, 1);
			geraldJap.text = "Hello?";
			robRap.text = "if\n(student == hungry) {\neat();\n} else {\ndontEat();\n}";
			aText.text = "I'm starving";
			bText.text = "Hungry";
			key = "Y";
		} else if (key == "Y") {
			geraldJap.text = "I'm starving.";
			robRap.text = "starvin != hungry\ndontEat();";
			aText.text = "What!?";
			optionB.transform.localScale = new Vector3 (0, 0, 0);
			key = "A";
			player.ChangeConf (-20);
		} else if (key == "A") {
			geraldJap.text = "What!?\nI'm hungry!\nHungry! Hungry!\nHungry!";
			robRap.text = "student == wimp\nshutDown();";
			optionA.transform.localScale = new Vector3 (0, 0, 0); 
			key = "end";
		} else if (key == "B") {
			geraldJap.text = "Thanks.\n...but what\nis this?";
			robRap.text = "SWEET\nGELATIN!!!";
			optionA.transform.localScale = new Vector3 (0, 0, 0);
			key = "end";
		}
	}
	/// <summary>
	/// runs conversation with right key
	/// </summary>
	/// <param name="B">B.</param>
	private void optionBClicked (Button B){
		if (key == "X") {
			geraldBubble.transform.localScale = new Vector3 (0.6f, 0.6f, 1);
			geraldJap.text = "Whoa!";
			robRap.text = "if (student == hungry) {\neat();\n} else {\ndontEat();\n}";
			aText.text = "I'm starving";
			bText.text = "Hungry";
			key = "Y";
		} else if (key == "Y") {
			geraldJap.text = "Hungry,\nI'm hungry.";
			robRap.text = "hungry == hungry\neat();";
			aText.text = "Thanks";
			optionB.transform.localScale = new Vector3 (0, 0, 0);
			key = "B";
			player.ChangeConf (10);
		}
	}
	/// <summary>
	/// entering collider hides inventory, shows canvases
	/// </summary>
	/// <param name="batman">Batman.</param>
	private void OnTriggerEnter (Collider batman) {
		if (batman.tag == "gerald") {
			inventoryCan.enabled = false;
			geraldVas.enabled = true;
			robVas.enabled = true;
		}
	}
	/// <summary>
	/// exiting collider shows inventory, hides canvases	
	/// </summary>
	/// <param name="batman">Batman.</param>
	private void OnTriggerExit (Collider batman) {
		if (batman.tag == "gerald") {
			inventoryCan.enabled = true;
			geraldVas.enabled = false;
			robVas.enabled = false;
			if (key == "end") {
				robCol.transform.localScale = new Vector3 (0, 0, 0);
				done.transform.localScale = new Vector3 (1, 1, 1);
			}
		}
	}
}
