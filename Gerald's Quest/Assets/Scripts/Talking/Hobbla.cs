﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Hobbla : MonoBehaviour {

	private GameObject playerScore;
	private Player player;
	private Canvas inventoryCan;
	private Canvas walterVas;
	private Button optionA;
	private Button optionB;
	private RawImage walterBubble;
	private Text waltering;
	private Text aText;
	private Text bText;
	private Canvas geraldVas;
	private RawImage geraldBubble;
	private Text geraldJap;
	private string key;
	private int noYell;

	void Start () {

		playerScore = GameObject.Find ("PlayerScore");
		player = playerScore.GetComponent<Player> ();

		inventoryCan = (Canvas)GameObject.Find ("Canvas").GetComponent<Canvas> ();
		walterVas = (Canvas)GameObject.Find ("walterVas").GetComponent<Canvas> ();
		walterBubble = (RawImage)GameObject.Find ("walterBubble").GetComponent<RawImage> ();
		waltering = (Text)GameObject.Find ("waltering").GetComponent<Text> ();
		optionA = (Button)GameObject.Find("optionA").GetComponent<Button>();
		optionB = (Button)GameObject.Find("optionB").GetComponent<Button>();

		geraldVas = (Canvas)GameObject.Find ("geraldVas").GetComponent<Canvas> ();
		geraldBubble = (RawImage)GameObject.Find ("geraldBubble").GetComponent<RawImage> ();
		geraldJap = (Text)GameObject.Find ("geraldJap").GetComponent<Text> ();
		aText = (Text)GameObject.Find ("aText").GetComponent<Text> ();
		bText = (Text)GameObject.Find ("bText").GetComponent<Text> ();

		optionA.onClick.AddListener (() => optionAClicked (optionA));
		optionB.onClick.AddListener (() => optionBClicked (optionB));

		walterVas.enabled = false;
		geraldVas.enabled = false;
		geraldBubble.transform.localScale = new Vector3 (0, 0, 0);
		optionA.transform.localScale = new Vector3 (0, 0, 0);
		optionB.transform.localScale = new Vector3 (0, 0, 0);

		key = "X";
		noYell = 0;
	}
	/// <summary>
	/// Hides buttons.
	/// </summary>
	public void End(){
		optionA.transform.localScale = new Vector3 (0, 0, 0);
		optionB.transform.localScale = new Vector3 (0, 0, 0);
		noYell++;
	}
	/// <summary>
	/// Runs right dialogue with right key value.
	/// </summary>
	/// <param name="one">One.</param>
	private void optionAClicked(Button one){
		if (key == "X") {        // 1 //
			geraldBubble.transform.localScale = new Vector3 (1, 1, 1);
			geraldJap.text = "Me?";
			waltering.text = "Yes, You.\nWho else?";
			aText.text = "Jesus?";
			bText.text = "Oh,\nsorry...";
			key = "A";
		} else if (key == "A") {  // 2 //
			geraldJap.text = "Jesus?";
			waltering.text = "Really?\nGood day...\n(weirdo)";
			End ();
			player.ChangeConf (-10);
			//END//
		} else if (key == "B") {  // 3 //
			geraldJap.text = "...";
			waltering.text = "Are you\nalright?";
			aText.text = "Nope";
			bText.text = "...";
			key = "BA";
		} else if (key == "AB") {  // 4 //
			geraldJap.text = "Gerald...";
			waltering.text = "Nice\nto meet you.\nI'm Walter,\nthe principle.\nI'll see\nyou around.";
			End ();
			player.ChangeConf (10);
			//END//
		} else if (key == "BA") {  // 5 //
			geraldJap.text = "Nope.";
			waltering.text = "I know\nhow you feel,\nbut get it\ntogether man.\nYou can do it.";
			End ();
			player.ChangeConf (5);
			//END//
		}
	}
	/// <summary>
	/// Runs right dialogue with right key value.
	/// </summary>
	/// <param name="two">Two.</param>
	private void optionBClicked(Button two){
		if (key == "X") {       // 1 //
			geraldBubble.transform.localScale = new Vector3 (1, 1, 1);
			geraldJap.text = "...";
			waltering.text = "...";
			aText.text = "...";
			bText.text = "...bye.";
			key = "B";
		} else if (key == "A") {  // 2 //
			geraldJap.text = "Oh,\nsorry...";
			waltering.text = "No worries.\n...so who\nare you?";
			aText.text = "Gerald...";
			bText.text = "Minding my\nown business";
			key = "AB";
		} else if (key == "B") {  // 3 //
			geraldJap.text = "...bye";
			waltering.text = "Well...\nI didn't\nwant to\ntalk to you\nanyways.";
			End ();
			player.ChangeConf (-5);
			//END//
		} else if (key == "AB") {  // 4 //
			geraldJap.text = "I'm Mr.\nMinding My Own\nBusiness.";
			waltering.text = "I'll make\nyour days here\na living hell.";
			End ();
			player.ChangeConf (-15);
			//END//
		} else if (key == "BA") {  // 5 //
			geraldJap.text = "...";
			waltering.text = "...fantastic.\nYet another\nhallucination.\nI better go\ntake my pills...";
			End ();
			player.ChangeConf (5);
			//END//
		}
	}
	/// <summary>
	/// When enttering collider hides canvas and if right key activates dialogue start, if not right key only enables canvases.
	/// </summary>
	/// <param name="batman">Batman.</param>
	private void OnTriggerEnter (Collider batman) {
		if (batman.tag == "gerald") {
			inventoryCan.enabled = false;
			if (key == "X") {
				optionA.transform.localScale = new Vector3 (0.3f, 0.3f, 1);
				optionB.transform.localScale = new Vector3 (0.3f, 0.3f, 1);
				waltering.text = "What do we\nhave here?";
				aText.text = "Me?";
				bText.text = "...";
				walterVas.enabled = true;
				geraldVas.enabled = true;
			} else {
				walterVas.enabled = true;
				geraldVas.enabled = true;
			}
		}
	}
	/// <summary>
	/// On collider exit shows inventory, if right value hides canvases, else activates npc yelling.
	/// </summary>
	/// <param name="batman">Batman.</param>
	private void OnTriggerExit (Collider batman) {
		if (batman.tag == "gerald") {
			inventoryCan.enabled = true;
			if (noYell == 1) {
				walterVas.enabled = false;
				geraldVas.enabled = false;
			} else {
				geraldVas.enabled = false;
				waltering.text = "Oi!!!\nGet back here!";
				key = "X";
				aText.text = "Me?";
				bText.text = "...";
				optionA.transform.localScale = new Vector3 (0, 0, 0);
				optionB.transform.localScale = new Vector3 (0, 0, 0);
			}
		}
	}
}
