﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OhNo : MonoBehaviour {

	private GameObject playerScore;
	private Player player;
	private GameObject ohNo;
	private float check;

	void Start () {

		ohNo = GameObject.Find ("ohNo");
		playerScore = GameObject.Find ("PlayerScore");
		player = playerScore.GetComponent<Player> ();

		check = player.GetConf ();
		//if more than 25 conf, hides trumpper
		if (check > 25) {
			ohNo.transform.localScale = new Vector3 (0, 0, 0);
		}

	}
	

}
