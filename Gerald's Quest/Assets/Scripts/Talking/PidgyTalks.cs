﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class PidgyTalks : MonoBehaviour {

	private GameObject playerScore;
	private Player player;

	private Canvas inventoryCan;
	private Canvas pidgyVas;
	private Canvas geraldVas;
	private Button option1;
	private Button option2;
	private Button option3;
	private Button option4;
	private Text pidgyRap;
	private Text geraldJap;
	private RawImage pidgyBubble;
	private int times;
	private Rigidbody pidgy;
	private Text text1;
	private Text text2;
	private Text text3;
	private Text text4;


	void Start () {

		playerScore = GameObject.Find ("PlayerScore");
		player = playerScore.GetComponent<Player> ();
		player.ChangeConf (10);

		inventoryCan = (Canvas)GameObject.Find ("Canvas").GetComponent<Canvas> ();
		geraldVas = (Canvas)GameObject.Find ("geraldVas").GetComponent<Canvas> ();
		pidgyVas = (Canvas)GameObject.Find ("pidgyVas").GetComponent<Canvas> ();

		pidgyVas = (Canvas)GameObject.Find ("pidgyVas").GetComponent<Canvas>();
		geraldVas = (Canvas)GameObject.Find ("geraldVas").GetComponent<Canvas>();

		pidgyVas.enabled = false;
		geraldVas.enabled = false;
		inventoryCan.enabled = true;

		option1 = (Button)GameObject.Find ("option1").GetComponent<Button> ();
		option2 = (Button)GameObject.Find ("option2").GetComponent<Button> ();
		option3 = (Button)GameObject.Find ("option3").GetComponent<Button> ();
		option4 = (Button)GameObject.Find ("option4").GetComponent<Button> ();
		pidgyBubble = (RawImage)GameObject.Find ("pidgyBubble").GetComponent<RawImage> ();
		pidgyRap = (Text)GameObject.Find ("pidgyRap").GetComponent<Text> ();
		geraldJap = (Text)GameObject.Find ("geraldJap").GetComponent<Text> ();

		text1 = (Text)GameObject.Find ("text1").GetComponent<Text> ();
		text2 = (Text)GameObject.Find ("text2").GetComponent<Text> ();
		text3 = (Text)GameObject.Find ("text3").GetComponent<Text> ();
		text4 = (Text)GameObject.Find ("text4").GetComponent<Text> ();


		option1.onClick.AddListener (() => option1Clicked (option1));
		option2.onClick.AddListener (() => option2Clicked (option2));
		option3.onClick.AddListener (() => option3Clicked (option3));
		option4.onClick.AddListener (() => option4Clicked (option4));

		pidgyBubble.enabled = false;
		pidgyRap.enabled = false;

		pidgyVas.enabled = false;
		geraldVas.enabled = false;

		pidgy = (Rigidbody)GameObject.Find ("pidgy").GetComponent<Rigidbody> ();

		times = 0;



	}
	/// <summary>
	/// Runs converstion with times pressed buttons
	/// </summary>
	/// <param name="one">One.</param>
	private void option1Clicked(Button one){
		if (times == 0) {
			geraldVas.enabled = true;
			geraldJap.text = "Oh, hi there. \n Who are you?";
			pidgyRap.text = "\n Plululululululul!!!";
			text1.text = "I am...";
			text2.text = "How are you?";
			times++;

		} else if (times == 1) {
			geraldJap.text = "I'm Gerald... \n ...and I'm talking to a pidgeon... \n Damn...";
			text1.text = "Why?...";
			text2.text = "...bye.";
			times++;

		} else if (times == 2) {
			geraldJap.text = "DAMN! \n What's wrong with me?";
			option1.transform.localScale = new Vector3 (0, 0, 0);
			option2.transform.localScale = new Vector3 (0, 0, 0);
			option3.transform.localScale = new Vector3 (0, 0, 0);
			option4.transform.localScale = new Vector3 (0.4f,0.2f,1);
			text4.text = "\n Well...";
			times++;
		}

	}
	/// <summary>
	/// Runs converstion with times pressed buttons
	/// </summary>
	/// <param name="two">Two.</param>
	private void option2Clicked(Button two) {
		if (times == 0) {
			pidgyRap.text = "\n Plululululululul!!!";
			pidgy.useGravity = true;
			times++;
			player.ChangeConf (10);
		} else if (times == 1) {
			geraldJap.text = "How is the morning treating you?";
			pidgyRap.text = "\n Plup! \n Lupuluul!"; 
			text1.text = "Damn!";
			text2.text = "Good bye.";
			option3.transform.localScale = new Vector3 (0.4f, 0.2f, 1);
			text3.text = "What are you looking at?!";
			times++;

		} else if (times == 2) {
			geraldJap.text = "See you around, \n though you all look the same, \n so how would I know.";
			pidgyRap.text = "\n Plulululululul! \n (That's racist dude!)";
			option1.transform.localScale = new Vector3 (0, 0, 0);
			option2.transform.localScale = new Vector3 (0, 0, 0);
			option3.transform.localScale = new Vector3 (0, 0, 0);
			option4.transform.localScale = new Vector3 (0, 0, 0);
			times++;
			player.ChangeConf (-5);

		}

	}
	/// <summary>
	/// Runs converstion with times pressed buttons
	/// </summary>
	/// <param name="three">Three.</param>
	private void option3Clicked(Button three){
		if (times == 2) {
			option1.transform.localScale = new Vector3 (0, 0, 0);
			option2.transform.localScale = new Vector3 (0, 0, 0);
			option3.transform.localScale = new Vector3 (0, 0, 0);
			option4.transform.localScale = new Vector3 (0, 0, 0);
			geraldJap.text = "Stop staring me! \n ";
			pidgyRap.text = "\n Plululul \n ulululul!!!";
			pidgy.useGravity = true;
			times++;
			player.ChangeConf (5);
		}
	}
	/// <summary>
	/// Runs converstion with times pressed buttons
	/// </summary>
	/// <param name="four">Four.</param>
	private void option4Clicked(Button four){
		if (times == 3) {
			option1.transform.localScale = new Vector3 (0, 0, 0);
			option2.transform.localScale = new Vector3 (0, 0, 0);
			option3.transform.localScale = new Vector3 (0, 0, 0);
			option4.transform.localScale = new Vector3 (0, 0, 0);
			geraldJap.text = "\n Goodbye birdie";
			pidgyRap.text = "\n Plululululul \n ulul!!! \n (Please, leave me alone.)";
			times++;
			player.ChangeConf (-5);
		}
	}
	/// <summary>
	/// entering collider hides inventory, shows pidgy canvas, if buttons not pressed: hides two buttons
	/// </summary>
	/// <param name="batman">Batman.</param>
	private void OnTriggerEnter (Collider batman) {
		if (batman.tag == "gerald") {
			inventoryCan.enabled = false;
			pidgyVas.enabled = true;
			pidgyBubble.enabled = true;
			pidgyRap.enabled = true;
			if (times == 0) {
				option3.transform.localScale = new Vector3 (0, 0, 0);
				option4.transform.localScale = new Vector3 (0, 0, 0);
			}

		}
	}
	/// <summary>
	/// exiting collider shows inventory, hides canvases
	/// </summary>
	/// <param name="batman">Batman.</param>
	private void OnTriggerExit (Collider batman) {
		if (batman.tag == "gerald") {
			inventoryCan.enabled = true;
			pidgyVas.enabled = false;
			pidgyBubble.enabled = false;
			pidgyRap.enabled = false;
			geraldVas.enabled = false;
		}
	}

}
