﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreakStuff : MonoBehaviour {

	private Rigidbody falseButton;
	private Rigidbody falseBar;
	private Canvas geraldVas;

	void Start () {
	
		geraldVas = (Canvas)GameObject.Find ("geraldVas").GetComponent<Canvas> ();
		falseButton = (Rigidbody)GameObject.Find ("falseButton").GetComponent<Rigidbody> ();
		falseBar = (Rigidbody)GameObject.Find ("falseBar").GetComponent<Rigidbody> ();

		geraldVas.enabled = false;
	}
	/// <summary>
	/// On enttering collider activates gravity in rigidbody.
	/// </summary>
	/// <param name="batman">Batman.</param>
	private void OnTriggerEnter (Collider batman) {
		if (batman.tag == "gerald") {
			falseButton.useGravity = true;
		}
	}
	/// <summary>
	/// On exiting collider activates gravity in rigidbody and enables canvas.
	/// </summary>
	/// <param name="batman">Batman.</param>
	private void OnTriggerExit (Collider batman) {
		if (batman.tag == "gerald") {
			falseBar.useGravity = true;
			geraldVas.enabled = true;
		}
	}
}
