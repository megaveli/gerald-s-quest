﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TehEnd : MonoBehaviour {

	private bool closeEnough;
	/// <summary>
	/// Loads scene when mouse relised, if bool true.
	/// </summary>
	private void OnMouseUp(){
		if (closeEnough == true) {
			SceneManager.LoadScene ("Credits");
		}
	}
	/// <summary>
	/// When enttering cillider gets bool true.
	/// </summary>
	/// <param name="batman">Batman.</param>
	private void OnTriggerEnter (Collider batman) {
		if (batman.tag == "gerald") {
			closeEnough = true;
		}
	}
	/// <summary>
	/// When exiting collider gets bool false.
	/// </summary>
	/// <param name="batman">Batman.</param>
	private void OnTriggerExit (Collider batman) {
		if (batman.tag == "gerald") {
			closeEnough = false;
		}
	}
}
