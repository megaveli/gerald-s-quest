﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AfterCafeteria : MonoBehaviour {
	/// <summary>
	/// Loads scene when enttered collider.
	/// </summary>
	/// <param name="batman">Batman.</param>
	private void OnTriggerEnter (Collider batman) {
		if (batman.tag == "gerald") {
			SceneManager.LoadScene ("BackToSchoolHall");
		}
	}
}
