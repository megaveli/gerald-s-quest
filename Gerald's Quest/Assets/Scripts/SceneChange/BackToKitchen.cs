﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BackToKitchen : MonoBehaviour {
	/// <summary>
	/// Loads scene if enttered collider.
	/// </summary>
	/// <param name="batman">Batman.</param>
	private void OnTriggerEnter (Collider batman){
		SceneManager.LoadScene ("BackAtHome");
	}
}
