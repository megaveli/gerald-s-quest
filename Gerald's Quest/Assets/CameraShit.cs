﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Camera script. Variables for min and max position for the camera
/// </summary>
public class CameraShit : MonoBehaviour {

	public Transform player;
	public Vector2 minPosition;
	public Vector2 maxPosition;

	/// <summary>
	/// Update late.
	/// </summary>
	void LateUpdate(){
		if (player != null) {
			float newX = Mathf.Clamp(player.position.x,minPosition.x,maxPosition.x);
			float newY = Mathf.Clamp(player.position.y,minPosition.y,maxPosition.y);
			transform.position = new Vector3(newX,newY,transform.position.z);		
		}
	}
}