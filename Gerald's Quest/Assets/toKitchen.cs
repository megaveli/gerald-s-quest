﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
/// <summary>
/// To kitchen. Script for changing the scene
/// </summary>
public class toKitchen : MonoBehaviour {

	/// <summary>
	/// Raises the mouse up event.
	/// Changes the scene
	/// </summary>
	private void OnMouseUp() {
		SceneManager.LoadScene ("Kitchen");
	}
}
