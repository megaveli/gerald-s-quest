﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Stop Megaveli scene after 3 seconds and changes to the StartMenu scene.
/// </summary>
public class stopMegaVeli : MonoBehaviour {

	/// <summary>
	/// Start this instance.
	/// </summary>
	void Start () {

		StartCoroutine (Stop ());
	}

	/// <summary>
	/// Stop this scene after 3 seconds.
	/// </summary>
	IEnumerator Stop() {
		yield return new WaitForSeconds (3);
		SceneManager.LoadScene ("StartMenu");
	}
}