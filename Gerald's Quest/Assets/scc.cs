﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Scc. For changing a scene
/// </summary>
public class scc : MonoBehaviour {

	/// <summary>
	/// Raises the mouse up event. Changes scene when clicked.
	/// </summary>
	private void OnMouseUp () {
		SceneManager.LoadScene ("StartMenu");
	}
}
