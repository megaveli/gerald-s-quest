using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class geraldMover : MonoBehaviour {

	public float speed;
	public bool canMove = true;
	public float _test;

	float _targetX;
	bool _moving;

	void Update() {
        if (canMove) {
            if (Input.GetMouseButtonDown(0)) {
                _targetX = Camera.main.ScreenToWorldPoint(Input.mousePosition).x;
                _moving = true;
                if (_targetX < transform.position.x)
                {
                    _targetX += _test;
                } else if (_targetX > transform.position.x)
                {
                    _targetX -= _test;
                }
            }
        }
			

			if (_moving) {
				if (Mathf.Abs(_targetX - transform.position.x) >= speed * Time.deltaTime)
					transform.position = Vector3.MoveTowards(transform.position, new Vector3(_targetX, transform.position.y), Time.deltaTime * speed);
				else {
					transform.position = new Vector3(_targetX, transform.position.y, transform.position.z);
					_moving = false;
				}
			}
		}
	}
